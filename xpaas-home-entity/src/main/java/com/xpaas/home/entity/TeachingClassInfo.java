package com.xpaas.home.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 教学班信息表实体类
 *
 * @author xPaas
 * @since 2022-02-18
 */
@Data
@TableName("SX_TEACHING_CLASS_INFO")
@ApiModel(value = "TeachingClassInfo对象", description = "教学班信息表")
public class TeachingClassInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	* 数据ID
	*/
		@ApiModelProperty(value = "数据ID")
		@TableId("ID")
	private String id;
	/**
	* 教学班名称
	*/
		@ApiModelProperty(value = "教学班名称")
		@TableField("TEACHING_CLASS_NAME")
	private String teachingClassName;
	/**
	* 开课教研室
	*/
		@ApiModelProperty(value = "开课教研室")
		@TableField("STAFF_ROOM")
	private String staffRoom;
	/**
	* 课程名称
	*/
		@ApiModelProperty(value = "课程名称")
		@TableField("COURSE_NAME")
	private String courseName;
	/**
	* 课程简称
	*/
		@ApiModelProperty(value = "课程简称")
		@TableField("COURSE_SIMPLE_NAME")
	private String courseSimpleName;
	/**
	* 课程英文名称
	*/
		@ApiModelProperty(value = "课程英文名称")
		@TableField("COURSE_EN_NAME")
	private String courseEnName;
	/**
	* 学分
	*/
		@ApiModelProperty(value = "学分")
		@TableField("CREDIT")
	private String credit;
	/**
	* 讲授学时
	*/
		@ApiModelProperty(value = "讲授学时")
		@TableField("TEACHING_HOURS")
	private String teachingHours;
	/**
	* 实践学时
	*/
		@ApiModelProperty(value = "实践学时")
		@TableField("PRACTICE_HOURS")
	private String practiceHours;
	/**
	* 总学时
	*/
		@ApiModelProperty(value = "总学时")
		@TableField("TOTAL_HOURS")
	private String totalHours;
	/**
	* 学科分类
	*/
		@ApiModelProperty(value = "学科分类")
		@TableField("SUBJECT_TYPE")
	private String subjectType;
	/**
	* 适用培养层次
	*/
		@ApiModelProperty(value = "适用培养层次")
		@TableField("EDU_LEVEL")
	private String eduLevel;
	/**
	* 课程负责人
	*/
		@ApiModelProperty(value = "课程负责人")
		@TableField("COURSE_CHARGER")
	private String courseCharger;
	/**
	* 课程负责人职工号
	*/
		@ApiModelProperty(value = "课程负责人职工号")
		@TableField("CHARGER_ACCOUNT")
	private String chargerAccount;
	/**
	* 课程评价
	*/
		@ApiModelProperty(value = "课程评价")
		@TableField("COURSE_EVALUATE")
	private String courseEvaluate;
	/**
	* 是否为主干课
	*/
		@ApiModelProperty(value = "是否为主干课")
		@TableField("MAIN_SUBJECT")
	private String mainSubject;
	/**
	* 课程类型
	*/
		@ApiModelProperty(value = "课程类型")
		@TableField("COURSE_TYPE")
	private String courseType;
	/**
	* 分制
	*/
		@ApiModelProperty(value = "分制")
		@TableField("POINTS_SYSTEM")
	private String pointsSystem;
	/**
	* 课下作业所占比例
	*/
		@ApiModelProperty(value = "课下作业所占比例")
		@TableField("HOMEWORK_RATIO")
	private String homeworkRatio;
	/**
	* 课堂测试所占比例
	*/
		@ApiModelProperty(value = "课堂测试所占比例")
		@TableField("HSKE_RATIO")
	private String hskeRatio;
	/**
	* 期中考试所占比例
	*/
		@ApiModelProperty(value = "期中考试所占比例")
		@TableField("MID_EXAM_RATIO")
	private String midExamRatio;
	/**
	* 期末考试所占比例
	*/
		@ApiModelProperty(value = "期末考试所占比例")
		@TableField("FINAL_EXAM_RETIO")
	private String finalExamRetio;
	/**
	* 考试成绩一票否决
	*/
		@ApiModelProperty(value = "考试成绩一票否决")
		@TableField("GRADE_ONE_VETO")
	private String gradeOneVeto;
	/**
	* 课程主页
	*/
		@ApiModelProperty(value = "课程主页")
		@TableField("COURSE_HOMEPAGE")
	private String courseHomepage;
	/**
	* 课程标准
	*/
		@ApiModelProperty(value = "课程标准")
		@TableField("COURSE_STANDARD")
	private String courseStandard;
	/**
	* 先修课程
	*/
		@ApiModelProperty(value = "先修课程")
		@TableField("PREREQUISITE_COURSE")
	private String prerequisiteCourse;
	/**
	* 课程简介
	*/
		@ApiModelProperty(value = "课程简介")
		@TableField("COURSE_INTRO")
	private String courseIntro;
	/**
	* 备注
	*/
		@ApiModelProperty(value = "备注")
		@TableField("REMARK")
	private String remark;
	/**
	* 修习性质
	*/
		@ApiModelProperty(value = "修习性质")
		@TableField("COURSE_PROPERTY")
	private String courseProperty;
	/**
	* 学期
	*/
		@ApiModelProperty(value = "学期")
		@TableField("TERM")
	private String term;
	/**
	* 实际上课人数
	*/
		@ApiModelProperty(value = "实际上课人数")
		@TableField("STUDENT_NUMBER")
	private String studentNumber;
	/**
	* 课时量填报状态
	*/
		@ApiModelProperty(value = "课时量填报状态")
		@TableField("CLASS_HOURS_FILL_STATUS")
	private String classHoursFillStatus;
	/**
	* 授课方式
	*/
		@ApiModelProperty(value = "授课方式")
		@TableField("TEACHING_METHOD")
	private String teachingMethod;
	/**
	* 排课要求
	*/
		@ApiModelProperty(value = "排课要求")
		@TableField("ARRANGE_REQ")
	private String arrangeReq;
	@TableField("TENANT_ID")
	private String tenantId;
	@TableField("CREATE_USER")
	private BigDecimal createUser;
	@TableField("CREATE_TIME")
	private LocalDateTime createTime;
	@TableField("UPDATE_USER")
	private BigDecimal updateUser;
	@TableField("UPDATE_TIME")
	private LocalDateTime updateTime;
	@TableField("STATUS")
	private BigDecimal status;
	@TableField("IS_DELETED")
	private BigDecimal isDeleted;
	@TableField("CREATE_DEPT")
	private String createDept;


}
