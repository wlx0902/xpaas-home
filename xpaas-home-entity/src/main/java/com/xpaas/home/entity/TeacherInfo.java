package com.xpaas.home.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 教员表实体类
 *
 * @author xPaas
 * @since 2022-02-10
 */
@Data
@TableName("SYS_TEACHER_INFO")
@ApiModel(value = "TeacherInfo对象", description = "教员表")
public class TeacherInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据id
     */
    @ApiModelProperty(value = "数据id")
    @TableId("ID")
    private String id;

    /**
     * 账户ID
     */
    @ApiModelProperty(value = "数据id")
    @TableId("XPAAS_ID")
    private String xpaasId;

    /**
     * 工作部门
     */
    @ApiModelProperty(value = "工作部门")
    @TableField("GZBM")
    private String gzbm;
    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    @TableField("TX")
    private String tx;
    /**
     * 数字签名
     */
    @ApiModelProperty(value = "数字签名")
    @TableField("SZQM")
    private String szqm;
    /**
     * 编制部门
     */
    @ApiModelProperty(value = "编制部门")
    @TableField("BZBM")
    private String bzbm;
    /**
     * 职务
     */
    @ApiModelProperty(value = "职务")
    @TableField("ZW")
    private String zw;
    /**
     * 类别
     */
    @ApiModelProperty(value = "类别")
    @TableField("LB")
    private String lb;
    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    @TableField("BH")
    private String bh;
    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    @TableField("XM")
    private String xm;
    /**
     * 英文姓名
     */
    @ApiModelProperty(value = "英文姓名")
    @TableField("YWXM")
    private String ywxm;
    /**
     * 姓名拼音
     */
    @ApiModelProperty(value = "姓名拼音")
    @TableField("XMPY")
    private String xmpy;
    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    @TableField("XB")
    private String xb;
    /**
     * 民族
     */
    @ApiModelProperty(value = "民族")
    @TableField("MZ")
    private String mz;
    /**
     * 出生日期
     */
    @ApiModelProperty(value = "出生日期")
    @TableField("CSRQ")
    private String csrq;
    /**
     * 证件名称
     */
    @ApiModelProperty(value = "证件名称")
    @TableField("ZJMC")
    private String zjmc;
    /**
     * 证件号码
     */
    @ApiModelProperty(value = "证件号码")
    @TableField("ZJHM")
    private String zjhm;
    /**
     * 参加工作日期
     */
    @ApiModelProperty(value = "参加工作日期")
    @TableField("CJGZRQ")
    private String cjgzrq;
    /**
     * 岗位编制类别
     */
    @ApiModelProperty(value = "岗位编制类别")
    @TableField("GWBZLB")
    private String gwbzlb;
    /**
     * 来院日期
     */
    @ApiModelProperty(value = "来院日期")
    @TableField("LYRQ")
    private String lyrq;
    /**
     * 教龄
     */
    @ApiModelProperty(value = "教龄")
    @TableField("JL")
    private String jl;
    /**
     * 国别
     */
    @ApiModelProperty(value = "国别")
    @TableField("GB")
    private String gb;
    /**
     * 出生省份
     */
    @ApiModelProperty(value = "出生省份")
    @TableField("CSSF")
    private String cssf;
    /**
     * 籍贯
     */
    @ApiModelProperty(value = "籍贯")
    @TableField("JG")
    private String jg;
    /**
     * 户籍所在地
     */
    @ApiModelProperty(value = "户籍所在地")
    @TableField("HJSZD")
    private String hjszd;
    /**
     * 当前政治面貌
     */
    @ApiModelProperty(value = "当前政治面貌")
    @TableField("DQZZMM")
    private String dqzzmm;
    /**
     * 入党(团)时间
     */
    @ApiModelProperty(value = "入党(团)时间")
    @TableField("RDTSJ")
    private String rdtsj;
    /**
     * 血型
     */
    @ApiModelProperty(value = "血型")
    @TableField("XX")
    private String xx;
    /**
     * 身高
     */
    @ApiModelProperty(value = "身高")
    @TableField("SG")
    private String sg;
    /**
     * 婚姻状况
     */
    @ApiModelProperty(value = "婚姻状况")
    @TableField("HYZK")
    private String hyzk;
    /**
     * 身体状况
     */
    @ApiModelProperty(value = "身体状况")
    @TableField("STZK")
    private String stzk;
    /**
     * 既往病史
     */
    @ApiModelProperty(value = "既往病史")
    @TableField("JWBS")
    private String jwbs;
    /**
     * 档案所在单位
     */
    @ApiModelProperty(value = "档案所在单位")
    @TableField("DASZDW")
    private String daszdw;
    /**
     * 档案号
     */
    @ApiModelProperty(value = "档案号")
    @TableField("DAH")
    private String dah;
    /**
     * 家庭住址
     */
    @ApiModelProperty(value = "家庭住址")
    @TableField("JTZZ")
    private String jtzz;
    /**
     * 家庭电话
     */
    @ApiModelProperty(value = "家庭电话")
    @TableField("JTDH")
    private String jtdh;
    /**
     * 传真号码
     */
    @ApiModelProperty(value = "传真号码")
    @TableField("CZHM")
    private String czhm;
    /**
     * 通信地址
     */
    @ApiModelProperty(value = "通信地址")
    @TableField("TXDZ")
    private String txdz;
    /**
     * 移动电话
     */
    @ApiModelProperty(value = "移动电话")
    @TableField("YDDH")
    private String yddh;
    /**
     * 邮政编码
     */
    @ApiModelProperty(value = "邮政编码")
    @TableField("YZBM")
    private String yzbm;
    /**
     * 单位电话
     */
    @ApiModelProperty(value = "单位电话")
    @TableField("DWDH")
    private String dwdh;
    /**
     * 电子邮箱
     */
    @ApiModelProperty(value = "电子邮箱")
    @TableField("DZYX")
    private String dzyx;
    /**
     * 个人主页
     */
    @ApiModelProperty(value = "个人主页")
    @TableField("GRZY")
    private String grzy;
    /**
     * 学历
     */
    @ApiModelProperty(value = "学历")
    @TableField("XL")
    private String xl;
    /**
     * 学习专业
     */
    @ApiModelProperty(value = "学习专业")
    @TableField("XXZY")
    private String xxzy;
    /**
     * 学位
     */
    @ApiModelProperty(value = "学位")
    @TableField("XW")
    private String xw;
    /**
     * 毕业日期
     */
    @ApiModelProperty(value = "毕业日期")
    @TableField("BYRQ")
    private String byrq;
    /**
     * 懂何种外语
     */
    @ApiModelProperty(value = "懂何种外语")
    @TableField("DHZWY")
    private String dhzwy;
    /**
     * 口译能力
     */
    @ApiModelProperty(value = "口译能力")
    @TableField("KYNL")
    private String kynl;
    /**
     * 毕业学院
     */
    @ApiModelProperty(value = "毕业学院")
    @TableField("BYXY")
    private String byxy;
    /**
     * 最后学历毕业日期
     */
    @ApiModelProperty(value = "最后学历毕业日期")
    @TableField("ZHXLBYRQ")
    private String zhxlbyrq;
    /**
     * 获得学位学科
     */
    @ApiModelProperty(value = "获得学位学科")
    @TableField("HDXWXK")
    private String hdxwxk;
    /**
     * 学制
     */
    @ApiModelProperty(value = "学制")
    @TableField("XZ")
    private String xz;
    /**
     * 翻译能力
     */
    @ApiModelProperty(value = "翻译能力")
    @TableField("FYNL")
    private String fynl;
    /**
     * 原部职别
     */
    @ApiModelProperty(value = "原部职别")
    @TableField("YBZB")
    private String ybzb;
    /**
     * 军街
     */
    @ApiModelProperty(value = "军街")
    @TableField("JJ")
    private String jj;
    /**
     * 军授予文号
     */
    @ApiModelProperty(value = "军授予文号")
    @TableField("JSYWH")
    private String jsywh;
    /**
     * 入伍日期
     */
    @ApiModelProperty(value = "入伍日期")
    @TableField("RWRQ")
    private String rwrq;
    /**
     * 军衔授予日期
     */
    @ApiModelProperty(value = "军衔授予日期")
    @TableField("JXSYRQ")
    private String jxsyrq;
    /**
     * 工作岗位
     */
    @ApiModelProperty(value = "工作岗位")
    @TableField("GZGW")
    private String gzgw;
    /**
     * 行政级别
     */
    @ApiModelProperty(value = "行政级别")
    @TableField("XZJB")
    private String xzjb;
    /**
     * 技术等级
     */
    @ApiModelProperty(value = "技术等级")
    @TableField("JSDJ")
    private String jsdj;
    /**
     * 党内任职
     */
    @ApiModelProperty(value = "党内任职")
    @TableField("DNRZ")
    private String dnrz;
    /**
     * 行政任职
     */
    @ApiModelProperty(value = "行政任职")
    @TableField("XZRZ")
    private String xzrz;
    /**
     * 参加何种学会
     */
    @ApiModelProperty(value = "参加何种学会")
    @TableField("CJHZXH")
    private String cjhzxh;
    /**
     * 在学会任何职
     */
    @ApiModelProperty(value = "在学会任何职")
    @TableField("ZXHRHZ")
    private String zxhrhz;
    /**
     * 技术职称
     */
    @ApiModelProperty(value = "技术职称")
    @TableField("JSZC")
    private String jszc;
    /**
     * 技术职称获得时间
     */
    @ApiModelProperty(value = "技术职称获得时间")
    @TableField("JSZCHDSJ")
    private String jszchdsj;
    /**
     * 政职职称
     */
    @ApiModelProperty(value = "政职职称")
    @TableField("ZZZC")
    private String zzzc;
    /**
     * 聘用职称
     */
    @ApiModelProperty(value = "聘用职称")
    @TableField("PYZC")
    private String pyzc;
    /**
     * 聘用职称日期
     */
    @ApiModelProperty(value = "聘用职称日期")
    @TableField("PYZCRQ")
    private String pyzcrq;
    /**
     * 在职
     */
    @ApiModelProperty(value = "在职")
    @TableField("ZZ")
    private String zz;
    /**
     * 在岗
     */
    @ApiModelProperty(value = "在岗")
    @TableField("ZG")
    private String zg;
    /**
     * 兼职
     */
    @ApiModelProperty(value = "兼职")
    @TableField("JZ")
    private String jz;
    /**
     * 现从事专业
     */
    @ApiModelProperty(value = "现从事专业")
    @TableField("XCSZY")
    private String xcszy;
    /**
     * 行政别获得时间
     */
    @ApiModelProperty(value = "行政别获得时间")
    @TableField("XZBHDSJ")
    private String xzbhdsj;
    /**
     * 技术等级获得时间
     */
    @ApiModelProperty(value = "技术等级获得时间")
    @TableField("JSDJHDSJ")
    private String jsdjhdsj;
    /**
     * 党内任职开始时间
     */
    @ApiModelProperty(value = "党内任职开始时间")
    @TableField("DNRZKSSJ")
    private String dnrzkssj;
    /**
     * 行政任职开始时间
     */
    @ApiModelProperty(value = "行政任职开始时间")
    @TableField("XZRZKSSJ")
    private String xzrzkssj;
    /**
     * 参加学会开始时间
     */
    @ApiModelProperty(value = "参加学会开始时间")
    @TableField("CJXHKSSJ")
    private String cjxhkssj;
    /**
     * 在学会任职开始时间
     */
    @ApiModelProperty(value = "在学会任职开始时间")
    @TableField("ZXHRZKSSJ")
    private String zxhrzkssj;
    /**
     * 职称批准文号
     */
    @ApiModelProperty(value = "职称批准文号")
    @TableField("ZCPZWH")
    private String zcpzwh;
    /**
     * 教学资格
     */
    @ApiModelProperty(value = "教学资格")
    @TableField("JXZG")
    private String jxzg;
    /**
     * 行政职称获得时间
     */
    @ApiModelProperty(value = "行政职称获得时间")
    @TableField("XZZCHDSJ")
    private String xzzchdsj;
    /**
     * 聘用职称批准文号
     */
    @ApiModelProperty(value = "聘用职称批准文号")
    @TableField("PYZCPZWH")
    private String pyzcpzwh;
    /**
     * 不在职原因
     */
    @ApiModelProperty(value = "不在职原因")
    @TableField("BZZYY")
    private String bzzyy;
    /**
     * 不在岗原因
     */
    @ApiModelProperty(value = "不在岗原因")
    @TableField("BZGYY")
    private String bzgyy;
    /**
     * 主要社会兼职
     */
    @ApiModelProperty(value = "主要社会兼职")
    @TableField("ZYSHJZ")
    private String zyshjz;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    @TableField("BZ")
    private String bz;

    /**
     * 入职时间
     */
    @ApiModelProperty(value = "入职时间")
    @TableField("RZSJ")
    private String rzsj;
    /**
     * 人员类别
     */
    @ApiModelProperty(value = "人员类别")
    @TableField("RWLB")
    private String rwlb;
    /**
     * 所属组织
     */
    @ApiModelProperty(value = "所属组织")
    @TableField("SSZZ")
    private String sszz;

    /**
     * 技术职务
     */
    @ApiModelProperty(value = "技术职务")
    @TableField("JSZW")
    private String jszw;
    /**
     * 技级资格
     */
    @ApiModelProperty(value = "技级资格")
    @TableField("JJZG")
    private String jszg;
    /**
     * 技职时间
     */
    @ApiModelProperty(value = "技职时间")
    @TableField("JZSJ")
    private String jssj;

    /**
     * 资格时间
     */
    @ApiModelProperty(value = "资格时间")
    @TableField("ZGSJ")
    private String zgsj;
    /**
     * 图片
     */
    @ApiModelProperty(value = "图片")
    @TableField("TP")
    private String tp;
    /**
     * 系
     */
    @ApiModelProperty(value = "系")
    @TableField("XI")
    private String xi;
    /**
     * 教研室
     */
    @ApiModelProperty(value = "教研室")
    @TableField("JYS")
    private String jys;


}
