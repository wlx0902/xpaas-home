package com.xpaas.home.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户基本信息表实体类
 *
 * @author xPaas
 * @since 2022-01-19
 */
@Data
@TableName("SYS_USER_DETAILS")
@ApiModel(value = "UserDetails对象", description = "用户基本信息表")
public class UserDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id")
    @TableField("ID")
    private Integer id;
    /**
     * 账号
     */
    @ApiModelProperty(value = "账号")
    @TableField("ACCOUNT")
    private String account;
    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    @TableField("NAME")
    private String name;
    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    @TableField("SEX")
    private String sex;
    /**
     * 单位 或 班级
     */
    @ApiModelProperty(value = "单位 或 班级")
    @TableField("UNIT")
    private String unit;
    /**
     * 职称
     */
    @ApiModelProperty(value = "职称")
    @TableField("JOB")
    private String job;
    /**
     * 头像
     */
    @ApiModelProperty(value = "头像")
    @TableField("HEADPORTAIT")
    private String headportait;
    /**
     * 签字照片
     */
    @ApiModelProperty(value = "签字照片")
    @TableField("SIGN")
    private String sign;
    /**
     * 身份标识 0表示教职员工，1表示学生
     */
    @ApiModelProperty(value = "身份标识 0表示教职员工，1表示学生")
    @TableField("IDENTITY")
    private String identity;


    @ApiModelProperty(value = "UID 关联XPAAS_USER主键ID")
    @TableField("U_ID")
    private String uid;

    @ApiModelProperty(value = "大队")
    @TableField("TEAM")
    private String team;

    @ApiModelProperty(value = "系部")
    @TableField("XB")
    private String xb;

    @ApiModelProperty(value = "学员队")
    @TableField("STUDENTTEAM")
    private String studentteam;

    @ApiModelProperty(value = "教研室")
    @TableField("CLASSROOM")
    private String classroom;
}
