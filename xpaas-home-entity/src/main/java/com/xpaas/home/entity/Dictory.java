package com.xpaas.home.entity;

import lombok.Data;

@Data
public class Dictory {

    private String code;

    private String value;

}
