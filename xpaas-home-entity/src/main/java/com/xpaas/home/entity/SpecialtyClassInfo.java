package com.xpaas.home.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 专业班次信息表实体类
 *
 * @author xPaas
 * @since 2022-02-09
 */
@Data
@TableName("JH_SPECIALTY_CLASS_INFO")
@ApiModel(value = "SpecialtyClassInfo对象", description = "专业班次信息表")
public class SpecialtyClassInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	* 数据ID
	*/
		@ApiModelProperty(value = "数据ID")
		@TableId("ID")
	private String id;
	/**
	* 年度专业ID
	*/
		@ApiModelProperty(value = "年度专业ID")
		@TableField("YEAR_SPECIALTY_ID")
	private String yearSpecialtyId;
	/**
	* 专业名称
	*/
		@ApiModelProperty(value = "专业名称")
		@TableField("SPECIALTY_NAME")
	private String specialtyName;
	/**
	* 年度
	*/
		@ApiModelProperty(value = "年度")
		@TableField("YEAR")
	private String year;
	/**
	* 班级名称
	*/
		@ApiModelProperty(value = "班级名称")
		@TableField("CLASS_NAME")
	private String className;
	/**
	* 别名
	*/
		@ApiModelProperty(value = "别名")
		@TableField("ALIAS")
	private String alias;
	/**
	* 学员队
	*/
		@ApiModelProperty(value = "学员队")
		@TableField("STUDENT_TEAM")
	private String studentTeam;
	/**
	* 专业额定人数
	*/
		@ApiModelProperty(value = "专业额定人数")
		@TableField("RATED_NUMBER")
	private String ratedNumber;
	/**
	* 所在校区
	*/
		@ApiModelProperty(value = "所在校区")
		@TableField("CAMPUS")
	private String campus;
	/**
	* 固定上课教室
	*/
		@ApiModelProperty(value = "固定上课教室")
		@TableField("STUDY_ROOM")
	private String studyRoom;
	/**
	* 固定自习数室
	*/
		@ApiModelProperty(value = "固定自习数室")
		@TableField("SELF_STUDY_ROOM")
	private String selfStudyRoom;
	/**
	* 专业班次主页
	*/
		@ApiModelProperty(value = "专业班次主页")
		@TableField("HOMEPAGE")
	private String homepage;
	/**
	* 备注
	*/
		@ApiModelProperty(value = "备注")
		@TableField("REMARK")
	private String remark;

	@ApiModelProperty(value = "培养层次")
	@TableField("TRAINING_LEVEL")
		private String trainingLevel;

}
