package com.xpaas.home.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;

/**
 * 角色表实体类
 *
 * @author xPaas
 * @since 2022-05-19
 */
@Data
@Alias("role1")
@TableName("XPAAS_ROLE")
@ApiModel(value = "Role对象", description = "角色表")
public class Role implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	* 主键
	*/
		@ApiModelProperty(value = "主键")
		@TableField("ID")
	private Long id;
	/**
	* 租户ID
	*/
		@ApiModelProperty(value = "租户ID")
		@TableField("TENANT_ID")
	private String tenantId;
	/**
	* 父主键
	*/
		@ApiModelProperty(value = "父主键")
		@TableField("PARENT_ID")
	private Long parentId;
	/**
	* 角色名
	*/
		@ApiModelProperty(value = "角色名")
		@TableField("ROLE_NAME")
	private String roleName;
	/**
	* 排序
	*/
		@ApiModelProperty(value = "排序")
		@TableField("SORT")
	private Long sort;
	/**
	* 角色别名
	*/
		@ApiModelProperty(value = "角色别名")
		@TableField("ROLE_ALIAS")
	private String roleAlias;
	/**
	* 是否已删除
	*/
		@ApiModelProperty(value = "是否已删除")
		@TableField("IS_DELETED")
	private Long isDeleted;


}
