package com.xpaas.home.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;

/**
 * 机构表实体类
 *
 * @author xPaas
 * @since 2022-03-03
 */
@Data
@Alias("dept1")
@TableName("XPAAS_DEPT")
@ApiModel(value = "Dept对象", description = "机构表")
public class Dept implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	* 主键
	*/
		@ApiModelProperty(value = "主键")
		@TableField("ID")
	private Long id;
	/**
	* 租户ID
	*/
		@ApiModelProperty(value = "租户ID")
		@TableField("TENANT_ID")
	private String tenantId;
	/**
	* 父主键
	*/
		@ApiModelProperty(value = "父主键")
		@TableField("PARENT_ID")
	private Long parentId;
	/**
	* 祖级列表
	*/
		@ApiModelProperty(value = "祖级列表")
		@TableField("ANCESTORS")
	private String ancestors;
	/**
	* 部门类型
	*/
		@ApiModelProperty(value = "部门类型")
		@TableField("DEPT_CATEGORY")
	private Long deptCategory;
	/**
	* 部门名
	*/
		@ApiModelProperty(value = "部门名")
		@TableField("DEPT_NAME")
	private String deptName;
	/**
	* 部门全称
	*/
		@ApiModelProperty(value = "部门全称")
		@TableField("FULL_NAME")
	private String fullName;
	/**
	* 排序
	*/
		@ApiModelProperty(value = "排序")
		@TableField("SORT")
	private Long sort;
	/**
	* 备注
	*/
		@ApiModelProperty(value = "备注")
		@TableField("REMARK")
	private String remark;
	/**
	* 是否已删除
	*/
		@ApiModelProperty(value = "是否已删除")
		@TableField("IS_DELETED")
	private Long isDeleted;


	@TableField(exist = false)
	private List<Dept> chilrenDept = new ArrayList<>();


}
