package com.xpaas.home.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 学期表实体类
 *
 * @author xPaas
 * @since 2022-02-23
 */
@Data
@TableName("SYS_TERM")
@ApiModel(value = "Term对象", description = "学期表")
public class Term implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	* 数据ID
	*/
		@ApiModelProperty(value = "数据ID")
		@TableField("ID")
	private String id;
	/**
	* 学期名称
	*/
		@ApiModelProperty(value = "学期名称")
		@TableField("XQMC")
	private String xqmc;
	/**
	* 学期所在学年
	*/
		@ApiModelProperty(value = "学期所在学年")
		@TableField("XQSZXN")
	private String xqszxn;
	/**
	* 学期序号
	*/
		@ApiModelProperty(value = "学期序号")
		@TableField("XQXH")
	private String xqxh;
	/**
	* 学期开始日期
	*/
		@ApiModelProperty(value = "学期开始日期")
		@TableField("XQKSRQ")
	private String xqksrq;
	/**
	* 学期結束日期
	*/
		@ApiModelProperty(value = "学期結束日期")
		@TableField("XQJSRQ")
	private String xqjsrq;
	/**
	* 假期結束日期
	*/
		@ApiModelProperty(value = "假期結束日期")
		@TableField("JQJSRQ")
	private String jqjsrq;


}
