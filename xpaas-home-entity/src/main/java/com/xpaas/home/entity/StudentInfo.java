package com.xpaas.home.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 学员表实体类
 *
 * @author xPaas
 * @since 2022-02-09
 */
@Data
@TableName(value = "SYS_STUDENT_INFO", resultMap = "studentInfoResultMap")
@ApiModel(value = "StudentInfo对象", description = "学员表")
public class StudentInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据ID
     */
    @ApiModelProperty(value = "数据ID")
    @TableField("ID")
    private String id;
    /**
     * XPAAS_ID
     */
    @ApiModelProperty(value = "XPAAS_ID")
    @TableField("XPAAS_ID")
    private String xpaasId;
    /**
     * 学员姓名
     */
    @ApiModelProperty(value = "学员姓名")
    @TableField("STUDENT_NAME")
    private String studentName;
    /**
     * 学号
     */
    @ApiModelProperty(value = "学号")
    @TableField("STUDENT_CODE")
    private String studentCode;
    /**
     * 姓名拼音
     */
    @ApiModelProperty(value = "姓名拼音")
    @TableField("NAME_PINYIN")
    private String namePinyin;
    /**
     * 曾用名
     */
    @ApiModelProperty(value = "曾用名")
    @TableField("USED_NAME")
    private String usedName;
    /**
     * 学员类别
     */
    @ApiModelProperty(value = "学员类别")
    @TableField("STUDENT_TYPE")
    private String studentType;
    /**
     * 籍贯
     */
    @ApiModelProperty(value = "籍贯")
    @TableField("NATIVE_PLACE")
    private String nativePlace;
    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    @TableField("GENDER")
    private String gender;
    /**
     * 出生日期
     */
    @ApiModelProperty(value = "出生日期")
    @TableField("BIRTHDAY")
    private LocalDateTime birthday;
    /**
     * 国别
     */
    @ApiModelProperty(value = "国别")
    @TableField("COUNTRY")
    private String country;
    /**
     * 民族
     */
    @ApiModelProperty(value = "民族")
    @TableField("NATION")
    private String nation;
    /**
     * 来源地区
     */
    @ApiModelProperty(value = "来源地区")
    @TableField("SOURCE_AREA")
    private String sourceArea;
    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证")
    @TableField("ID_CARD")
    private String idCard;
    /**
     * 是否军人
     */
    @ApiModelProperty(value = "是否军人")
    @TableField("SOLDIER")
    private String soldier;
    /**
     * 军官证
     */
    @ApiModelProperty(value = "军官证")
    @TableField("JGZ")
    private String jgz;
    /**
     * 联系电话
     */
    @ApiModelProperty(value = "联系电话")
    @TableField("PHONE")
    private String phone;
    /**
     * 通信地址
     */
    @ApiModelProperty(value = "通信地址")
    @TableField("MAILING_ADDRESS")
    private String mailingAddress;
    /**
     * 入学日期
     */
    @ApiModelProperty(value = "入学日期")
    @TableField("BEGIN_DATE")
    private LocalDateTime beginDate;
    /**
     * 毕业日期
     */
    @ApiModelProperty(value = "毕业日期")
    @TableField("END_DATE")
    private LocalDateTime endDate;
    /**
     * 专业班次ID
     */
    @ApiModelProperty(value = "专业班次ID")
    @TableField("SPECIALTY_CLASS_ID")
    private String specialtyClassId;
    /**
     * 专业班次
     */
    @ApiModelProperty(value = "专业班次")
    @TableField("SPECIALTY_CLASS_NAME")
    private String specialtyClassName;
    /**
     * 政治面貌
     */
    @ApiModelProperty(value = "政治面貌")
    @TableField("POLITICS_FACE")
    private String politicsFace;
    /**
     * 生源省份
     */
    @ApiModelProperty(value = "生源省份")
    @TableField("STUDENT_PROVINCES")
    private String studentProvinces;
    /**
     * 办学类型
     */
    @ApiModelProperty(value = "办学类型")
    @TableField("SCHOOL_RUNNING_TYPE")
    private String schoolRunningType;
    /**
     * 学历证书号
     */
    @ApiModelProperty(value = "学历证书号")
    @TableField("EDU_CERTIFICATE_NO")
    private String eduCertificateNo;
    /**
     * 本科毕业时间
     */
    @ApiModelProperty(value = "本科毕业时间")
    @TableField("UNDERGRADUATE_END_DATE")
    private String undergraduateEndDate;
    /**
     * 学籍注册号
     */
    @ApiModelProperty(value = "学籍注册号")
    @TableField("REGISTER_NUMBER")
    private String registerNumber;
    /**
     * 批准书号
     */
    @ApiModelProperty(value = "批准书号")
    @TableField("APPROVAL_NUMBER")
    private String approvalNumber;
    /**
     * 班内序号
     */
    @ApiModelProperty(value = "班内序号")
    @TableField("NUMBER_IN_CLASS")
    private String numberInClass;
    /**
     * 学员队
     */
    @ApiModelProperty(value = "学员队")
    @TableField("STUDENT_DUI")
    private String studentDui;
    /**
     * 学位证书号
     */
    @ApiModelProperty(value = "学位证书号")
    @TableField("DEG_CERTIFICATE_NO")
    private String degCertificateNo;
    /**
     * 是否获取学位
     */
    @ApiModelProperty(value = "是否获取学位")
    @TableField("ISDEGREE")
    private String isdegree;
    /**
     * 学位
     */
    @ApiModelProperty(value = "学位")
    @TableField("DEGREE")
    private String degree;
    /**
     * 学历
     */
    @ApiModelProperty(value = "学历")
    @TableField("EDU")
    private String edu;
    /**
     * 学历证发放专业
     */
    @ApiModelProperty(value = "学历证发放专业")
    @TableField("MAJOR_NAME")
    private String majorName;
    /**
     * 学历证发放院校
     */
    @ApiModelProperty(value = "学历证发放院校")
    @TableField("SCHOOL_NAME")
    private String schoolName;
    /**
     * 照片
     */
    @ApiModelProperty(value = "照片")
    @TableField("PHOTO")
    private String photo;
    /**
     * 毕结业
     */
    @ApiModelProperty(value = "毕结业")
    @TableField("GRADUATIION")
    private String graduatiion;
    /**
     * 学籍院校
     */
    @ApiModelProperty(value = "学籍院校")
    @TableField("SCHOOL")
    private String school;
    /**
     * 学籍专业
     */
    @ApiModelProperty(value = "学籍专业")
    @TableField("ZYBH")
    private String zybh;
    /**
     * 学籍培养目标
     */
    @ApiModelProperty(value = "学籍培养目标")
    @TableField("PYLEVEL")
    private String pylevel;
    /**
     * 军人证件代码
     */
    @ApiModelProperty(value = "军人证件代码")
    @TableField("CRET_CODE")
    private String cretCode;
    /**
     * 军人证件类型
     */
    @ApiModelProperty(value = "军人证件类型")
    @TableField("CRET_TYPE")
    private String cretType;
    /**
     * 毕结业时间
     */
    @ApiModelProperty(value = "毕结业时间")
    @TableField("GRADUATIION_TIME")
    private LocalDateTime graduatiionTime;
    /**
     * 军人类型
     */
    @ApiModelProperty(value = "军人类型")
    @TableField("SOLDIER_TYPE")
    private String soldierType;
    /**
     * 是否删除(0-未删，1-删除)
     */
    @ApiModelProperty(value = "是否删除(0-未删，1-删除)")
    @TableField("DEL_FLAG")
    private String delFlag;
    /**
     * 特殊授位 1.特殊授位 0.正常授位
     */
    @ApiModelProperty(value = "特殊授位 1.特殊授位 0.正常授位")
    @TableField("SP_DEGREE")
    private BigDecimal spDegree;
    /**
     * 0:正常毕业,1:特殊毕业
     */
    @ApiModelProperty(value = "0:正常毕业,1:特殊毕业")
    @TableField("SP_GRADUATION")
    private String spGraduation;
    /**
     * 专业名称
     */
    @ApiModelProperty(value = "专业名称")
    @TableField("SPECIALTYNAME")
    private String specialtyname;
    /**
     * 专业ID
     */
    @ApiModelProperty(value = "专业ID")
    @TableField("SPECIALTYID")
    private String specialtyid;
    /**
     * 学制
     */
    @ApiModelProperty(value = "学制")
    @TableField("BZXZ")
    private String bzxz;

    @TableField(exist = false)
    private int tol;


    @TableField(exist = false)
    private SpecialtyClassInfo specialtyClassInfo;

}
