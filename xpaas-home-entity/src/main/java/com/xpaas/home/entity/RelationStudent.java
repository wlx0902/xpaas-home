package com.xpaas.home.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类
 *
 * @author xPaas
 * @since 2022-02-11
 */
@Data
@TableName("KC_RELATION_STUDENT")
@ApiModel(value = "RelationStudent对象", description = "RelationStudent对象")
public class RelationStudent implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	* 主键ID
	*/
		@ApiModelProperty(value = "主键ID")
		@TableId("ID")
	private BigDecimal id;
	/**
	* KC_EXAMPALAN主键ID
	*/
		@ApiModelProperty(value = "KC_EXAMPALAN主键ID")
		@TableField("E_ID")
	private String eId;
	/**
	* SYS_STUDENT_ID主键ID
	*/
		@ApiModelProperty(value = "SYS_STUDENT_ID主键ID")
		@TableField("S_ID")
	private String sId;


}
