package com.xpaas.home.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;

/**
 * 用户表实体类
 *
 * @author xPaas
 * @since 2022-02-28
 */
@Data
@TableName(value = "XPAAS_USER",resultMap = "userResultMap")
@Alias("users")
@ApiModel(value = "User对象", description = "用户表")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	* 主键
	*/
		@ApiModelProperty(value = "主键")
		@TableId("ID")
	private Long id;
	/**
	* 租户ID
	*/
		@ApiModelProperty(value = "租户ID")
		@TableField("TENANT_ID")
	private String tenantId;
	/**
	* 账号
	*/
		@ApiModelProperty(value = "账号")
		@TableField("ACCOUNT")
	private String account;
	/**
	* 密码
	*/
		@ApiModelProperty(value = "密码")
		@TableField("PASSWORD")
	private String password;
	/**
	* 昵称
	*/
		@ApiModelProperty(value = "昵称")
		@TableField("NAME")
	private String name;
	/**
	* 真名
	*/
		@ApiModelProperty(value = "真名")
		@TableField("REAL_NAME")
	private String realName;
	/**
	* 头像
	*/
		@ApiModelProperty(value = "头像")
		@TableField("AVATAR")
	private String avatar;
	/**
	* 邮箱
	*/
		@ApiModelProperty(value = "邮箱")
		@TableField("EMAIL")
	private String email;
	/**
	* 手机
	*/
		@ApiModelProperty(value = "手机")
		@TableField("PHONE")
	private String phone;
	/**
	* 生日
	*/
		@ApiModelProperty(value = "生日")
		@TableField("BIRTHDAY")
	private LocalDateTime birthday;
	/**
	* 性别
	*/
		@ApiModelProperty(value = "性别")
		@TableField("SEX")
	private Integer sex;
	/**
	* 角色id
	*/
		@ApiModelProperty(value = "角色id")
		@TableField("ROLE_ID")
	private String roleId;
	/**
	* 部门id
	*/
		@ApiModelProperty(value = "部门id")
		@TableField("DEPT_ID")
	private String deptId;
	/**
	* 创建人
	*/
		@ApiModelProperty(value = "创建人")
		@TableField("CREATE_USER")
	private Long createUser;
	/**
	* 创建部门
	*/
		@ApiModelProperty(value = "创建部门")
		@TableField("CREATE_DEPT")
	private Long createDept;
	/**
	* 创建时间
	*/
		@ApiModelProperty(value = "创建时间")
		@TableField("CREATE_TIME")
	private LocalDateTime createTime;
	/**
	* 修改人
	*/
		@ApiModelProperty(value = "修改人")
		@TableField("UPDATE_USER")
	private Long updateUser;
	/**
	* 修改时间
	*/
		@ApiModelProperty(value = "修改时间")
		@TableField("UPDATE_TIME")
	private LocalDateTime updateTime;
	/**
	* 状态
	*/
		@ApiModelProperty(value = "状态")
		@TableField("STATUS")
	private Long status;
	/**
	* 是否已删除
	*/
		@ApiModelProperty(value = "是否已删除")
		@TableField("IS_DELETED")
	private Long isDeleted;
	/**
	* 职位id
	*/
		@ApiModelProperty(value = "职位id")
		@TableField("JOB_ID")
	private Long jobId;
	/**
	* 请假标准
	*/
		@ApiModelProperty(value = "请假标准")
		@TableField("LEAVE_DAYS")
	private String leaveDays;
	/**
	* 年龄
	*/
		@ApiModelProperty(value = "年龄")
		@TableField("AGE")
	private String age;
	/**
	* 证件号
	*/
		@ApiModelProperty(value = "证件号")
		@TableField("ID_NUMBER")
	private String idNumber;
	/**
	* 入伍日期
	*/
		@ApiModelProperty(value = "入伍日期")
		@TableField("INRANKS_TIME")
	private LocalDateTime inranksTime;
	/**
	* 政治面貌
	*/
		@ApiModelProperty(value = "政治面貌")
		@TableField("OUTLOOK")
	private String outlook;
	/**
	* 文化程度
	*/
		@ApiModelProperty(value = "文化程度")
		@TableField("DEGREE")
	private String degree;
	/**
	* 籍贯
	*/
		@ApiModelProperty(value = "籍贯")
		@TableField("PLACE")
	private String place;
	/**
	* 民族
	*/
		@ApiModelProperty(value = "民族")
		@TableField("NATION")
	private String nation;
	/**
	* 军衔
	*/
		@ApiModelProperty(value = "军衔")
		@TableField("RANK_TYPE")
	private String rankType;
	/**
	* 人员状态
	*/
		@ApiModelProperty(value = "人员状态")
		@TableField("USER_TYPE")
	private String userType;
	/**
	* 路途次数
	*/
		@ApiModelProperty(value = "路途次数")
		@TableField("WAY_NUMBER")
	private String wayNumber;
	/**
	* 实休天数
	*/
		@ApiModelProperty(value = "实休天数")
		@TableField("PLEASE_NUMBER")
	private String pleaseNumber;
	/**
	* 职级
	*/
		@ApiModelProperty(value = "职级")
		@TableField("RANK_ID")
	private String rankId;
	/**
	* 人脸照片
	*/
		@ApiModelProperty(value = "人脸照片")
		@TableField("PICTURE_FACE")
	private String pictureFace;
	/**
	* 婚姻(1、已婚，2、未婚)
	*/
		@ApiModelProperty(value = "婚姻(1、已婚，2、未婚)")
		@TableField("MARRIAGE")
	private String marriage;
	/**
	* 是否重点人员(1、是，0、否)
	*/
		@ApiModelProperty(value = "是否重点人员(1、是，0、否)")
		@TableField("EMPHASIS_USER")
	private String emphasisUser;
	/**
	* 证件号
	*/
		@ApiModelProperty(value = "证件号")
		@TableField("ID_NUMBERS")
	private String idNumbers;
	@TableField("WAY_STAND")
	private String wayStand;
// ------------隔离----------------------------------------------------------
	@TableField(exist = false)
	private TeacherInfo teacherInfo;

	@TableField(exist = false)
	private String roleName;

}
