package com.xpaas.home.entity;

import lombok.Data;

/**
 * 渲染用
 */
@Data
public class ListUsers {

    private Integer id;
    private String name;

}
