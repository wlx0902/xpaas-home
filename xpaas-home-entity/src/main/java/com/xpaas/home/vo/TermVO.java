package com.xpaas.home.vo;

import com.xpaas.home.entity.Term;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 学期表视图实体类
 *
 * @author xPaas
 * @since 2022-02-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "TermVO对象", description = "学期表")
public class TermVO extends Term {
	private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "创建人")
    private String createUserName;
    @ApiModelProperty(value = "修改人")
    private String updateUserName;
}
