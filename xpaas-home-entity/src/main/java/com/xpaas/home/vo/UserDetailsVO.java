package com.xpaas.home.vo;

import com.xpaas.home.entity.ListUsers;
import com.xpaas.home.entity.UserDetails;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * 用户基本信息表视图实体类
 *
 * @author xPaas
 * @since 2022-01-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserDetailsVO对象", description = "用户基本信息表")
public class UserDetailsVO extends UserDetails {
	private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "创建人")
    private String createUserName;
    @ApiModelProperty(value = "修改人")
    private String updateUserName;

    List<ListUsers> mtList;
    List<ListUsers> pyList;
    List<ListUsers> stList;

}
