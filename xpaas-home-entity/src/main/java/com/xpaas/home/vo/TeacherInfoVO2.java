package com.xpaas.home.vo;

import lombok.Data;

/**
 * @author Wangluoxin
 * @date 2022/2/11
 */
@Data
public class TeacherInfoVO2 {
    private String id;
    private String name;
    private String sex;
    private String unit;
    private String job;
}
