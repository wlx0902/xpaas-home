package com.xpaas.home.vo;

import lombok.Data;

//施训选修课开课管理添加学员查询条件入参
@Data
public class SxAddStudentRC {

    private String pylevelid;//培养层次id

    private String studentduiid;//学员队id

    private String grade;//年级

    private String zyid;//专业id

    private String studentcode;//学号

    private String studentname;//姓名

}
