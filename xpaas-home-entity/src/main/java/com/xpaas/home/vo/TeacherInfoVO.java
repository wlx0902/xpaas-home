package com.xpaas.home.vo;

import com.xpaas.home.entity.TeacherInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 教员表视图实体类
 *
 * @author xPaas
 * @since 2022-02-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "TeacherInfoVO对象", description = "教员表")
public class TeacherInfoVO extends TeacherInfo {
	private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "创建人")
    private String createUserName;
    @ApiModelProperty(value = "修改人")
    private String updateUserName;



}
