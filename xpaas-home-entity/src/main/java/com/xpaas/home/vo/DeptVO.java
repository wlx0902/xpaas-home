package com.xpaas.home.vo;

import com.xpaas.home.entity.Dept;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 机构表视图实体类
 *
 * @author xPaas
 * @since 2022-03-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "DeptVO对象", description = "机构表")
public class DeptVO extends Dept {
	private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "创建人")
    private String createUserName;
    @ApiModelProperty(value = "修改人")
    private String updateUserName;
}
