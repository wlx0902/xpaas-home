package com.xpaas.home.vo;

import lombok.Data;

/**
 * @author Wangluoxin
 * @date 2022/2/11
 */
@Data
public class TeacherInfoVO3 {
    private String id;
    private String name;
    private String zjhm;
    private String unit;
    private String job;
}
