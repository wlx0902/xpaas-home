package com.xpaas.home.vo;

import com.xpaas.core.secure.LoginUser;
import com.xpaas.home.entity.StudentInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 学员表视图实体类
 *
 * @author xPaas
 * @since 2022-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "StudentInfoVO对象", description = "学员表")
public class StudentInfoVO extends StudentInfo {
	private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "创建人")
    private String createUserName;
    @ApiModelProperty(value = "修改人")
    private String updateUserName;


    private LoginUser loginUser;
}
