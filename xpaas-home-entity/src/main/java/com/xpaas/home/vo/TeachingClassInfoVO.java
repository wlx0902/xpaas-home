package com.xpaas.home.vo;

import com.xpaas.home.entity.TeachingClassInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 教学班信息表视图实体类
 *
 * @author xPaas
 * @since 2022-02-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "TeachingClassInfoVO对象", description = "教学班信息表")
public class TeachingClassInfoVO extends TeachingClassInfo {
	private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "创建人")
    private String createUserName;
    @ApiModelProperty(value = "修改人")
    private String updateUserName;
}
