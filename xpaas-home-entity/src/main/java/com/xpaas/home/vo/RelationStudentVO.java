package com.xpaas.home.vo;

import com.xpaas.home.entity.RelationStudent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 视图实体类
 *
 * @author xPaas
 * @since 2022-02-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "RelationStudentVO对象", description = "RelationStudentVO对象")
public class RelationStudentVO extends RelationStudent {
	private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "创建人")
    private String createUserName;
    @ApiModelProperty(value = "修改人")
    private String updateUserName;
}
