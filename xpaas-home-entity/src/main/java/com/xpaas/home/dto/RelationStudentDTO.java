package com.xpaas.home.dto;

import com.xpaas.home.entity.RelationStudent;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 数据传输对象实体类
 *
 * @author xPaas
 * @since 2022-02-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RelationStudentDTO extends RelationStudent {
	private static final long serialVersionUID = 1L;

}
