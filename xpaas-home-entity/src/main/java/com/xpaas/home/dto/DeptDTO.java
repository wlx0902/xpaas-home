package com.xpaas.home.dto;

import com.xpaas.home.entity.Dept;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 机构表数据传输对象实体类
 *
 * @author xPaas
 * @since 2022-03-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DeptDTO extends Dept {
	private static final long serialVersionUID = 1L;

}
