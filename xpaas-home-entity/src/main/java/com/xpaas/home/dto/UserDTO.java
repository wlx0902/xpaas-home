package com.xpaas.home.dto;

import com.xpaas.home.entity.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户表数据传输对象实体类
 *
 * @author xPaas
 * @since 2022-02-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserDTO extends User {
	private static final long serialVersionUID = 1L;

}
