package com.xpaas.home.dto;

import lombok.Data;

////施训选修课开课管理添加学员查询条件出参
@Data
public class SxAddStudentCC {


    private String studentCode;//学号

    private String studentName;//姓名

    private String gender;//性别

    private String grade;//年级

    private String zy;//专业

    private String studentdui;//学员队

    private String pylevel;//培养层次

    private String studentid;//学生id

    private String zybcid;//专业班次ID
}
