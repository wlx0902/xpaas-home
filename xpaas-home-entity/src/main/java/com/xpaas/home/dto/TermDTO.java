package com.xpaas.home.dto;

import com.xpaas.home.entity.Term;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 学期表数据传输对象实体类
 *
 * @author xPaas
 * @since 2022-02-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TermDTO extends Term {
	private static final long serialVersionUID = 1L;

}
