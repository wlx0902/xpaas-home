package com.xpaas.home.dto;

import com.xpaas.home.entity.TeacherInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 教员表数据传输对象实体类
 *
 * @author xPaas
 * @since 2022-02-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TeacherInfoDTO extends TeacherInfo {
	private static final long serialVersionUID = 1L;

}
