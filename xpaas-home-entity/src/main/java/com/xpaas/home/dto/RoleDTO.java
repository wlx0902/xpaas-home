package com.xpaas.home.dto;

import com.xpaas.home.entity.Role;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色表数据传输对象实体类
 *
 * @author xPaas
 * @since 2022-05-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RoleDTO extends Role {
	private static final long serialVersionUID = 1L;

}
