package com.xpaas.home.dto;

import com.xpaas.home.entity.TeachingClassInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 教学班信息表数据传输对象实体类
 *
 * @author xPaas
 * @since 2022-02-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TeachingClassInfoDTO extends TeachingClassInfo {
	private static final long serialVersionUID = 1L;

}
