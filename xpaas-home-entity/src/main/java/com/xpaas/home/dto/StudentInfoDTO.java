package com.xpaas.home.dto;

import com.xpaas.home.entity.StudentInfo;
import com.xpaas.home.entity.StudentInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 学员表数据传输对象实体类
 *
 * @author xPaas
 * @since 2022-02-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StudentInfoDTO extends StudentInfo {
	private static final long serialVersionUID = 1L;

}
