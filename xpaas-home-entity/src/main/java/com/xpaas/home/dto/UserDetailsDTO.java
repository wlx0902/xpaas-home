package com.xpaas.home.dto;

import com.xpaas.home.entity.UserDetails;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户基本信息表数据传输对象实体类
 *
 * @author xPaas
 * @since 2022-01-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserDetailsDTO extends UserDetails {
	private static final long serialVersionUID = 1L;

}
