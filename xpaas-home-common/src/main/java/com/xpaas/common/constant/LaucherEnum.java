package com.xpaas.common.constant;

import com.xpaas.common.launch.PropertiesDev;
import com.xpaas.common.launch.PropertiesProd;
import com.xpaas.common.launch.PropertiesTest;
import com.xpaas.core.launch.constant.AppConstant;

public enum LaucherEnum {
    INSTANCE;
    private PropertiesDev propertiesDev;
    private PropertiesTest propertiesTest;
    private PropertiesProd propertiesProd;

    private LaucherEnum() {
        propertiesDev = new PropertiesDev();
        propertiesTest = new PropertiesTest();
        propertiesProd = new PropertiesProd();
    }

    public ILaucher getInstance(String profile) {
        switch (profile) {
            case (AppConstant.PROD_CODE):
                return propertiesProd;
            case (AppConstant.TEST_CODE):
                return propertiesTest;
            default:
                return propertiesDev;
        }
    }
}
