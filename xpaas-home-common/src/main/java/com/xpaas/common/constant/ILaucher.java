package com.xpaas.common.constant;

public interface ILaucher {
    String getNacosAddr();

    String getSentinelAddr();

    String getSeataAddr();

    String getZipkinAddr();

    String getElkAddr();
}
