package com.xpaas.common.utils;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xpaas.core.mp.support.Query;
import org.springframework.util.CollectionUtils;


import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 分页插件， 没有做数据库底层分类 在集合做硬分类
 *
 * @author wlx
 */
public class PageUtil {

    private static PageUtil instance;

    public PageUtil() {
    }

    public static PageUtil build() {
        //检查是否要阻塞
        if (instance == null) {
            synchronized (PageUtil.class) {
                //检查是否要重新创建实例
                if (instance == null) {
                    instance = new PageUtil();
                }
            }
        }
        return instance;
    }

    /**
     * @param collections 实体集合
     * @param query       分页条件
     * @return page 返回前端page对象
     */
    public <T> IPage<T> getPage(Collection<T> collections, Query query) {
        IPage<T> page = new Page<>();
        if (CollectionUtils.isEmpty(collections)) {

            return page;
        }

        List<T> collect = collections.stream()
                .skip((long) (query.getCurrent() - 1) * query.getSize())
                .limit(query.getSize())
                .collect(Collectors.toList());
        page.setRecords(collect);
        page.setPages((collections.size() - 1) / page.getSize() + 1);
        page.setSize(query.getSize());
        page.setTotal(collections.size());
        page.setCurrent(query.getCurrent());
        return page;
    }
}
