package com.xpaas.common.launch;

import com.xpaas.common.constant.ILaucher;
import lombok.Getter;

@Getter
public class PropertiesTest implements ILaucher {
    /**
     * nacos test 测试地址
     */
    private String nacosAddr = "localhost:8848";
    /**
     * sentinel dev 地址
     */
    private  String sentinelAddr = "localhost:8858";
    /**
     * seata dev 地址
     */
    private  String seataAddr = "localhost:8091";
    /**
     * zipkin dev 地址
     */
    private  String zipkinAddr = "http://localhost:9411";
    /**
     * elk dev 地址
     */
    private String elkAddr = "localhost:9000";

}
