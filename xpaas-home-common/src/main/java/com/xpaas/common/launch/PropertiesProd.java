package com.xpaas.common.launch;

import com.xpaas.common.constant.ILaucher;
import lombok.Getter;

@Getter
public class PropertiesProd implements ILaucher {
    /**
     * nacos prod  生产地址
     */
    private String nacosAddr = "192.168.0.182:8848";
//    private String nacosAddr = "121.36.100.47:8848";
//	private String nacosAddr = "192.168.1.191:8848";
//	private String nacosAddr = "localhost:8848";
    /**
     * sentinel dev 地址
     */
    private  String sentinelAddr = "192.168.0.159:8858";
    /**
     * seata dev 地址
     */
    private  String seataAddr = "192.168.0.159:8091";
    /**
     * zipkin dev 地址
     */
    private  String zipkinAddr = "http://192.168.0.159:9411";
    /**
     * elk dev 地址
     */
    private String elkAddr = "127.0.0.1:9000";

}
