package com.xpaas.common.launch;

import com.xpaas.common.constant.ILaucher;
import lombok.Getter;

@Getter
public class PropertiesDev implements ILaucher {
    /**
     * nacos dev 开发地址
     */
    private String nacosAddr = "192.168.1.111:8848";
    /**
     * sentinel dev 地址
     */
    private  String sentinelAddr = "127.0.0.1:8858";
    /**
     * seata dev 地址
     */
    private  String seataAddr = "127.0.0.1:8091";
    /**
     * zipkin dev 地址
     */
    private  String zipkinAddr = "http://127.0.0.1:9411";
    /**
     * elk dev 地址
     */
    private String elkAddr = "127.0.0.1:9000";

}
