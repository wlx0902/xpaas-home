package com.xpaas.common.config;

import com.xpaas.core.secure.LoginUser;
import com.xpaas.core.secure.utils.AuthUtil;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;


//TODO 暂时存放
@Component
public class CurrentUserResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        //传递注解对象
        return methodParameter.hasParameterAnnotation(CurrentUser.class);
    }
    @Override
    public LoginUser resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        return AuthUtil.getUser();
    }
}
