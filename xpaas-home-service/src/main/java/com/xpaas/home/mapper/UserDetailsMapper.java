package com.xpaas.home.mapper;

import com.xpaas.home.entity.UserDetails;
import com.xpaas.home.vo.UserDetailsVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户基本信息表 Mapper 接口
 *
 * @author xPaas
 * @since 2022-01-19
 */
@Mapper
public interface UserDetailsMapper extends BaseMapper<UserDetails> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param userDetails
	 * @return
	 */
	List<UserDetailsVO> selectUserDetailsPage(IPage page, UserDetailsVO userDetails);

}
