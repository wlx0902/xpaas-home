package com.xpaas.home.mapper;

import com.xpaas.home.entity.User;
import com.xpaas.home.vo.UserVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表 Mapper 接口
 *
 * @author xPaas
 * @since 2022-02-28
 */
public interface UserMapper extends BaseMapper<User> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param user
	 * @return
	 */
	List<UserVO> selectUserPage(IPage page, UserVO user);

	List<String> getNumber(@Param("deptId") String deptId);



}
