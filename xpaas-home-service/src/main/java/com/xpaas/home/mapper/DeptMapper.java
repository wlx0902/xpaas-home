package com.xpaas.home.mapper;

import com.xpaas.home.entity.Dept;
import com.xpaas.home.vo.DeptVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 * 机构表 Mapper 接口
 *
 * @author xPaas
 * @since 2022-03-03
 */
public interface DeptMapper extends BaseMapper<Dept> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param dept
	 * @return
	 */
	List<DeptVO> selectDeptPage(IPage page, DeptVO dept);

}
