package com.xpaas.home.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.xpaas.home.entity.Term;
import com.xpaas.home.vo.TermVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 * 学期表 Mapper 接口
 *
 * @author xPaas
 * @since 2022-02-23
 */
@SqlParser(filter=true)
public interface TermMapper extends BaseMapper<Term> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param term
	 * @return
	 */
	List<TermVO> selectTermPage(IPage page, TermVO term);

}
