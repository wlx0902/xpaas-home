package com.xpaas.home.mapper;

import com.xpaas.home.entity.RelationStudent;
import com.xpaas.home.vo.RelationStudentVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 *  Mapper 接口
 *
 * @author xPaas
 * @since 2022-02-11
 */
public interface RelationStudentMapper extends BaseMapper<RelationStudent> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param relationStudent
	 * @return
	 */
	List<RelationStudentVO> selectRelationStudentPage(IPage page, RelationStudentVO relationStudent);

}
