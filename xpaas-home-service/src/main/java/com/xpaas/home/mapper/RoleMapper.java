package com.xpaas.home.mapper;

import com.xpaas.home.entity.Role;
import com.xpaas.home.vo.RoleVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 * 角色表 Mapper 接口
 *
 * @author xPaas
 * @since 2022-05-19
 */
public interface RoleMapper extends BaseMapper<Role> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param role
	 * @return
	 */
	List<RoleVO> selectRolePage(IPage page, RoleVO role);

}
