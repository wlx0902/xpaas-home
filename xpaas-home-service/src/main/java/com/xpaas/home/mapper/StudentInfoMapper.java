package com.xpaas.home.mapper;

import com.xpaas.home.dto.SxAddStudentCC;
import com.xpaas.home.entity.StudentInfo;
import com.xpaas.home.vo.StudentInfoVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.vo.SxAddStudentRC;

import java.util.List;

/**
 * 学员表 Mapper 接口
 *
 * @author xPaas
 * @since 2022-02-09
 */
public interface StudentInfoMapper extends BaseMapper<StudentInfo> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param studentInfo
	 * @return
	 */
	List<StudentInfoVO> selectStudentInfoPage(IPage page, StudentInfoVO studentInfo);

	List<StudentInfo> selectStudentByPlanid(String[] planids);
	/**
	 * 条件查询学生（施训选修课开课管理的添加学员查询）
	 * @param sxAddStudentRC
	 * @return
	 */
	List<SxAddStudentCC> querystudentinfobysxinfo(SxAddStudentRC sxAddStudentRC);

}
