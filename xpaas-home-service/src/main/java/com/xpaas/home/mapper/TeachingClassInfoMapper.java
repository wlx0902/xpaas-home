package com.xpaas.home.mapper;

import com.xpaas.home.entity.TeachingClassInfo;
import com.xpaas.home.vo.TeachingClassInfoVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 * 教学班信息表 Mapper 接口
 *
 * @author xPaas
 * @since 2022-02-18
 */
public interface TeachingClassInfoMapper extends BaseMapper<TeachingClassInfo> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param teachingClassInfo
	 * @return
	 */
	List<TeachingClassInfoVO> selectTeachingClassInfoPage(IPage page, TeachingClassInfoVO teachingClassInfo);
	/**
	 * 查询所有
	 * @return
	 */
	List<TeachingClassInfoVO> selectByAll();
}
