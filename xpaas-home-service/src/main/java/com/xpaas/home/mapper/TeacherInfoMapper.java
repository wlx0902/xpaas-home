package com.xpaas.home.mapper;

import com.xpaas.home.entity.TeacherInfo;
import com.xpaas.home.vo.TeacherInfoVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;

/**
 * 教员表 Mapper 接口
 *
 * @author xPaas
 * @since 2022-02-10
 */
public interface TeacherInfoMapper extends BaseMapper<TeacherInfo> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param teacherInfo
	 * @return
	 */
	List<TeacherInfoVO> selectTeacherInfoPage(IPage page, TeacherInfoVO teacherInfo);

}
