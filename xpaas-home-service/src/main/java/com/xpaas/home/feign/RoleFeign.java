package com.xpaas.home.feign;

import com.xpaas.system.entity.Role;
import lombok.Data;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author Wangluoxin
 * @date 2022/3/1
 */
@FeignClient("xpaas-system")
public interface RoleFeign {


    @GetMapping("/role/detail")
    Role getRole(Role role);


}
