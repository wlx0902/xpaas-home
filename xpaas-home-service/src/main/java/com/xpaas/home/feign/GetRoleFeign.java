package com.xpaas.home.feign;

import com.xpaas.home.entity.Role;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("xpaas-home-service")
@Primary
public interface GetRoleFeign {

    @GetMapping("/role/queryrolename")
    Role queryrolename(@RequestParam("ids")String ids);
}