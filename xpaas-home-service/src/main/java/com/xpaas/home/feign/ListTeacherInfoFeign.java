package com.xpaas.home.feign;


import com.xpaas.core.tool.api.R;
import com.xpaas.home.entity.TeacherInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "xpaas-home-service")
@Primary
public interface ListTeacherInfoFeign {


    /**
     * 通过当前Integer类型 主键类型
     * @param id
     * @return
     */
    @GetMapping("/teacherinfo/selectById")
    TeacherInfo selectById(@RequestParam("id") Object id);

    /**
     * 通过当前String类型 XPAAS_ID获取 用户信息
     * @param id
     * @return
     */
    @GetMapping("/teacherinfo/selectByIdStr")
    TeacherInfo selectByIdStr(@RequestParam("id") String id);


    @GetMapping("/teacherinfo/selectByUId")
    TeacherInfo selectByUId(@RequestParam("uid") String uid);


    /**
     * 获取当前用户的信息
     * @return
     */
    @GetMapping("/teacherinfo/getCurrentOneEntity")
    TeacherInfo getCurrentOneEntity();


    /**
     * 获取
     * @return
     */
    @GetMapping("/teacherinfo/getCurrentOne")
    R studentAndTeacher();

}
