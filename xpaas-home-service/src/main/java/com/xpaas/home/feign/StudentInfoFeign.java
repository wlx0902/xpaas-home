package com.xpaas.home.feign;

import com.xpaas.home.entity.StudentInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Wangluoxin
 * @date 2022/2/15
 */
@FeignClient("xpaas-home-service")
public interface StudentInfoFeign {


    /**
     * 获取SYS_STUDENT_INFO表中的学生信息
     * @return
     */
    @GetMapping("studentinfo/getCurrentOneEntity")
    StudentInfo getCurrentOneEntity();
}
