package com.xpaas.home.feign;

import com.xpaas.home.entity.UserDetails;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("xpaas-home-service")
public interface ListUserFeign {


    @GetMapping("/userdetails/selectById")
    UserDetails selectById(@RequestParam("id") Integer id);

    @GetMapping("/userdetails/selectByUId")
    UserDetails selectByUId(@RequestParam("uid") String uid);
}
