package com.xpaas.home.service.impl;

import com.xpaas.home.entity.TeachingClassInfo;
import com.xpaas.home.vo.TeachingClassInfoVO;
import com.xpaas.home.mapper.TeachingClassInfoMapper;
import com.xpaas.home.service.ITeachingClassInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 教学班信息表 服务实现类
 *
 * @author xPaas
 * @since 2022-02-18
 */
@Service
public class TeachingClassInfoServiceImpl extends ServiceImpl<TeachingClassInfoMapper, TeachingClassInfo> implements ITeachingClassInfoService {

	@Override
	public IPage<TeachingClassInfoVO> selectTeachingClassInfoPage(IPage<TeachingClassInfoVO> page, TeachingClassInfoVO teachingClassInfo) {
		return page.setRecords(baseMapper.selectTeachingClassInfoPage(page, teachingClassInfo));
	}

	@Override
	public List<TeachingClassInfoVO> selectByAll() {
		return baseMapper.selectByAll();
	}

}
