package com.xpaas.home.service;

import com.xpaas.home.entity.Dept;
import com.xpaas.home.vo.DeptVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 机构表 服务类
 *
 * @author xPaas
 * @since 2022-03-03
 */
public interface IDeptService extends IService<Dept> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param dept
	 * @return
	 */
	IPage<DeptVO> selectDeptPage(IPage<DeptVO> page, DeptVO dept);


	List<Dept> QueryDept();

}
