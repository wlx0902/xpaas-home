package com.xpaas.home.service.impl;

import com.xpaas.home.dto.SxAddStudentCC;
import com.xpaas.home.entity.StudentInfo;
import com.xpaas.home.vo.StudentInfoVO;
import com.xpaas.home.mapper.StudentInfoMapper;
import com.xpaas.home.service.IStudentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xpaas.home.vo.SxAddStudentRC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 学员表 服务实现类
 *
 * @author xPaas
 * @since 2022-02-09
 */
@Service
public class StudentInfoServiceImpl extends ServiceImpl<StudentInfoMapper, StudentInfo> implements IStudentInfoService {

	@Autowired
	StudentInfoMapper studentInfoMapper;
	@Override
	public IPage<StudentInfoVO> selectStudentInfoPage(IPage<StudentInfoVO> page, StudentInfoVO studentInfo) {
		return page.setRecords(baseMapper.selectStudentInfoPage(page, studentInfo));
	}

	@Override
	public List<StudentInfo> selectStudentByPlanid(String[] planids) {
		return studentInfoMapper.selectStudentByPlanid(planids);
	}

	@Override
	public List<SxAddStudentCC> querystudentinfobysxinfo(SxAddStudentRC sxAddStudentRC) {
		return studentInfoMapper.querystudentinfobysxinfo(sxAddStudentRC);
	}

}
