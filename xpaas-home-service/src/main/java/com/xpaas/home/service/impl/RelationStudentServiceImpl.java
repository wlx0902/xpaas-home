package com.xpaas.home.service.impl;

import com.xpaas.home.entity.RelationStudent;
import com.xpaas.home.vo.RelationStudentVO;
import com.xpaas.home.mapper.RelationStudentMapper;
import com.xpaas.home.service.IRelationStudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 *  服务实现类
 *
 * @author xPaas
 * @since 2022-02-11
 */
@Service
public class RelationStudentServiceImpl extends ServiceImpl<RelationStudentMapper, RelationStudent> implements IRelationStudentService {

	@Override
	public IPage<RelationStudentVO> selectRelationStudentPage(IPage<RelationStudentVO> page, RelationStudentVO relationStudent) {
		return page.setRecords(baseMapper.selectRelationStudentPage(page, relationStudent));
	}

}
