package com.xpaas.home.service;

import com.xpaas.home.entity.UserDetails;
import com.xpaas.home.vo.UserDetailsVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 用户基本信息表 服务类
 *
 * @author xPaas
 * @since 2022-01-19
 */
public interface IUserDetailsService extends IService<UserDetails> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param userDetails
	 * @return
	 */
	IPage<UserDetailsVO> selectUserDetailsPage(IPage<UserDetailsVO> page, UserDetailsVO userDetails);

	/**
	 * 渲染职工列表
	 * @return
	 */
	List<UserDetails> selectJobs(UserDetails userDetails);
}
