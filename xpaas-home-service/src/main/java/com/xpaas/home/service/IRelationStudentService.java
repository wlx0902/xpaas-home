package com.xpaas.home.service;

import com.xpaas.home.entity.RelationStudent;
import com.xpaas.home.vo.RelationStudentVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 *  服务类
 *
 * @author xPaas
 * @since 2022-02-11
 */
public interface IRelationStudentService extends IService<RelationStudent> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param relationStudent
	 * @return
	 */
	IPage<RelationStudentVO> selectRelationStudentPage(IPage<RelationStudentVO> page, RelationStudentVO relationStudent);

}
