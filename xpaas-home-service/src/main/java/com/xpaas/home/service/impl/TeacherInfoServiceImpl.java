package com.xpaas.home.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xpaas.home.entity.TeacherInfo;
import com.xpaas.home.entity.UserDetails;
import com.xpaas.home.vo.TeacherInfoVO;
import com.xpaas.home.mapper.TeacherInfoMapper;
import com.xpaas.home.service.ITeacherInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 教员表 服务实现类
 *
 * @author xPaas
 * @since 2022-02-10
 */
@Service
public class TeacherInfoServiceImpl extends ServiceImpl<TeacherInfoMapper, TeacherInfo> implements ITeacherInfoService {

	@Override
	public IPage<TeacherInfoVO> selectTeacherInfoPage(IPage<TeacherInfoVO> page, TeacherInfoVO teacherInfo) {
		return page.setRecords(baseMapper.selectTeacherInfoPage(page, teacherInfo));
	}

	@Override
	public List<TeacherInfo> selectJobs(TeacherInfo teacherInfo) {
		LambdaQueryWrapper<TeacherInfo> lambdaQueryWrapper=new LambdaQueryWrapper();
		if (teacherInfo.getBzbm()!=null&&!"".equals(teacherInfo.getBzbm()))
			lambdaQueryWrapper.eq(TeacherInfo::getBzbm,teacherInfo.getBzbm());
		if (teacherInfo.getXm()!=null&&!"".equals(teacherInfo.getXm()))
			lambdaQueryWrapper.like(TeacherInfo::getXm,teacherInfo.getXm());
		List<TeacherInfo> teacherInfos = baseMapper.selectList(lambdaQueryWrapper);
		return teacherInfos;
	}


}
