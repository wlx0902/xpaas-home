package com.xpaas.home.service;

import com.xpaas.home.entity.User;
import com.xpaas.home.vo.UserVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 用户表 服务类
 *
 * @author xPaas
 * @since 2022-02-28
 */
public interface IUserService extends IService<User> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param user
	 * @return
	 */
	IPage<UserVO> selectUserPage(IPage<UserVO> page, UserVO user);

}
