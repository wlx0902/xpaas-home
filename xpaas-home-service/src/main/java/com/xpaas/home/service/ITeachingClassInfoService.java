package com.xpaas.home.service;

import com.xpaas.home.entity.TeachingClassInfo;
import com.xpaas.home.vo.TeachingClassInfoVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 教学班信息表 服务类
 *
 * @author xPaas
 * @since 2022-02-18
 */
public interface ITeachingClassInfoService extends IService<TeachingClassInfo> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param teachingClassInfo
	 * @return
	 */
	IPage<TeachingClassInfoVO> selectTeachingClassInfoPage(IPage<TeachingClassInfoVO> page, TeachingClassInfoVO teachingClassInfo);

	/**
	 * 查询所有
	 * @return
	 */
	List<TeachingClassInfoVO> selectByAll();

}
