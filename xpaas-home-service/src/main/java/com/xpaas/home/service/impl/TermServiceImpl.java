package com.xpaas.home.service.impl;

import com.xpaas.home.entity.Term;
import com.xpaas.home.vo.TermVO;
import com.xpaas.home.mapper.TermMapper;
import com.xpaas.home.service.ITermService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 学期表 服务实现类
 *
 * @author xPaas
 * @since 2022-02-23
 */
@Service
public class TermServiceImpl extends ServiceImpl<TermMapper, Term> implements ITermService {

	@Override
	public IPage<TermVO> selectTermPage(IPage<TermVO> page, TermVO term) {
		return page.setRecords(baseMapper.selectTermPage(page, term));
	}

}
