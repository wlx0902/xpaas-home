package com.xpaas.home.service.impl;

import com.xpaas.home.entity.Role;
import com.xpaas.home.vo.RoleVO;
import com.xpaas.home.mapper.RoleMapper;
import com.xpaas.home.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 角色表 服务实现类
 *
 * @author xPaas
 * @since 2022-05-19
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

	@Override
	public IPage<RoleVO> selectRolePage(IPage<RoleVO> page, RoleVO role) {
		return page.setRecords(baseMapper.selectRolePage(page, role));
	}

}
