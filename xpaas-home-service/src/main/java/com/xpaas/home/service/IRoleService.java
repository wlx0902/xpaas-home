package com.xpaas.home.service;

import com.xpaas.home.entity.Role;
import com.xpaas.home.vo.RoleVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 角色表 服务类
 *
 * @author xPaas
 * @since 2022-05-19
 */
public interface IRoleService extends IService<Role> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param role
	 * @return
	 */
	IPage<RoleVO> selectRolePage(IPage<RoleVO> page, RoleVO role);

}
