package com.xpaas.home.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xpaas.core.tool.utils.Func;
import com.xpaas.home.entity.UserDetails;
import com.xpaas.home.vo.UserDetailsVO;
import com.xpaas.home.mapper.UserDetailsMapper;
import com.xpaas.home.service.IUserDetailsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户基本信息表 服务实现类
 *
 * @author xPaas
 * @since 2022-01-19
 */
@Service
public class UserDetailsServiceImpl extends ServiceImpl<UserDetailsMapper, UserDetails> implements IUserDetailsService {

	@Autowired
	UserDetailsMapper userDetailsMapper;

	@Override
	public IPage<UserDetailsVO> selectUserDetailsPage(IPage<UserDetailsVO> page, UserDetailsVO userDetails) {
		return page.setRecords(baseMapper.selectUserDetailsPage(page, userDetails));
	}

	@Override
	public List<UserDetails> selectJobs(UserDetails userDetails) {
		LambdaQueryWrapper<UserDetails> lambdaQueryWrapper=new LambdaQueryWrapper();
		//查询标识符为0是教职工的人员
		lambdaQueryWrapper.eq(UserDetails::getIdentity,0);
		if (userDetails.getUnit()!=null&&!userDetails.getUnit().equals(""))
			lambdaQueryWrapper.eq(UserDetails::getUnit,userDetails.getUnit());
		if (userDetails.getName()!=null&&!userDetails.getName().equals(""))
			lambdaQueryWrapper.like(UserDetails::getName,userDetails.getName());
		List<UserDetails> userDetailss = userDetailsMapper.selectList(lambdaQueryWrapper);
		return userDetailss;
	}

}
