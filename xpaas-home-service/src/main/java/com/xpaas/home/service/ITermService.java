package com.xpaas.home.service;

import com.xpaas.home.entity.Term;
import com.xpaas.home.vo.TermVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 学期表 服务类
 *
 * @author xPaas
 * @since 2022-02-23
 */
public interface ITermService extends IService<Term> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param term
	 * @return
	 */
	IPage<TermVO> selectTermPage(IPage<TermVO> page, TermVO term);

}
