package com.xpaas.home.service.impl;

import com.xpaas.home.entity.User;
import com.xpaas.home.vo.UserVO;
import com.xpaas.home.mapper.UserMapper;
import com.xpaas.home.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 用户表 服务实现类
 *
 * @author xPaas
 * @since 2022-02-28
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

	@Override
	public IPage<UserVO> selectUserPage(IPage<UserVO> page, UserVO user) {
		return page.setRecords(baseMapper.selectUserPage(page, user));
	}

}
