package com.xpaas.home.service;

import com.xpaas.home.entity.TeacherInfo;
import com.xpaas.home.entity.UserDetails;
import com.xpaas.home.vo.TeacherInfoVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 教员表 服务类
 *
 * @author xPaas
 * @since 2022-02-10
 */
public interface ITeacherInfoService extends IService<TeacherInfo> {

	/**
	 * 自定义分页
	 *
	 * @param page
	 * @param teacherInfo
	 * @return
	 */
	IPage<TeacherInfoVO> selectTeacherInfoPage(IPage<TeacherInfoVO> page, TeacherInfoVO teacherInfo);


	List<TeacherInfo> selectJobs(TeacherInfo teacherInfo);
}
