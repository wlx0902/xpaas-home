package com.xpaas.home.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.xpaas.home.entity.Dept;
import com.xpaas.home.vo.DeptVO;
import com.xpaas.home.mapper.DeptMapper;
import com.xpaas.home.service.IDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.ArrayList;
import java.util.List;

/**
 * 机构表 服务实现类
 *
 * @author xPaas
 * @since 2022-03-03
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements IDeptService {

	@Override
	public IPage<DeptVO> selectDeptPage(IPage<DeptVO> page, DeptVO dept) {
		return page.setRecords(baseMapper.selectDeptPage(page, dept));
	}

	@Override
	public List<Dept> QueryDept() {
		ArrayList<Dept> arrayList = new ArrayList<>();

		recursivelyFindCategoriesplace(arrayList,0L);

		return arrayList;
	}


	public void recursivelyFindCategoriesplace(List<Dept> deptList,Long parentid){


		QueryWrapper<Dept> q = new QueryWrapper();
		q.eq("PARENT_ID", parentid);

//        q.ne("TYPELEVE","0");
//		q.orderByAsc("XH");
		List<Dept> depts = baseMapper.selectList(q);

		if (!CollectionUtils.isEmpty(depts)) {
			for (int i = 0; i < depts.size(); i++) {
				Dept dept = depts.get(i);

				deptList.add(dept);
				recursivelyFindCategoriesplace(dept.getChilrenDept(),dept.getId());
			}
		}

	}

}
