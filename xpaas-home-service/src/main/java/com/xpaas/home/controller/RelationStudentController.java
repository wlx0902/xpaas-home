package com.xpaas.home.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xpaas.home.entity.StudentInfo;
import com.xpaas.home.service.IStudentInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import com.xpaas.core.mp.support.Condition;
import com.xpaas.core.mp.support.Query;
import com.xpaas.core.tool.api.R;
import com.xpaas.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.entity.RelationStudent;
import com.xpaas.home.vo.RelationStudentVO;
import com.xpaas.home.service.IRelationStudentService;
import com.xpaas.core.boot.ctrl.BaseController;

import java.util.ArrayList;
import java.util.List;

/**
 *  控制器
 *
 * @author xPaas
 * @since 2022-02-11
 */
@RestController
@AllArgsConstructor
@RequestMapping("/relationstudent")
@Api(value = "", tags = "接口")
public class RelationStudentController extends BaseController {

	private IRelationStudentService relationStudentService;

	private IStudentInfoService studentInfoService;
	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入relationStudent")
	public R<RelationStudent> detail(RelationStudent relationStudent) {
		RelationStudent detail = relationStudentService.getOne(Condition.getQueryWrapper(relationStudent));
		return R.data(detail);
	}

	/**
	 * 分页 
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入relationStudent")
	public R<IPage<RelationStudent>> list(RelationStudent relationStudent, Query query) {
		IPage<RelationStudent> pages = relationStudentService.page(Condition.getPage(query), Condition.getQueryWrapper(relationStudent));
		return R.data(pages);
	}

    /**
     * 自定义分页 
     */
    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "分页", notes = "传入relationStudent")
    public R<IPage<RelationStudentVO>> page(RelationStudentVO relationStudent, Query query) {
        IPage<RelationStudentVO> pages = relationStudentService.selectRelationStudentPage(Condition.getPage(query), relationStudent);
        return R.data(pages);
    }



	/**
	 * 新增 
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入relationStudent")
	public R save(@Valid @RequestBody RelationStudent relationStudent) {
		return R.status(relationStudentService.save(relationStudent));
	}

	/**
	 * 修改 
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入relationStudent")
	public R update(@Valid @RequestBody RelationStudent relationStudent) {
		return R.status(relationStudentService.updateById(relationStudent));
	}

	/**
	 * 新增或修改 
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入relationStudent")
	public R submit(@Valid @RequestBody RelationStudent relationStudent) {
		return R.status(relationStudentService.saveOrUpdate(relationStudent));
	}

	
	/**
	 * 删除 
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(relationStudentService.removeByIds(Func.toLongList(ids)));
	}


	@PostMapping("/selectCourseListOrStudent")
	@ApiOperationSupport(order = 9)
	@ApiOperation(value = "通过中间件表获取学生信息", notes = "传入考试课程ID")
	public R selectCourseListOrStudent(@RequestBody List<String> list){

//		List<RelationStudent> e_id = relationStudentService.
//				list(new QueryWrapper<RelationStudent>().in("E_ID", list));
//		List<String> stringList=new ArrayList<>();
//		for (RelationStudent studentNo : e_id) {
//			stringList.add(studentNo.getSId());
//		}
		System.out.println(list);
		List<StudentInfo> student_code = studentInfoService
				.list(new QueryWrapper<StudentInfo>().in("STUDENT_CODE", list));
		return R.data(student_code);
	}
}
