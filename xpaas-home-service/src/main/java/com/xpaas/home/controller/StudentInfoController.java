package com.xpaas.home.controller;

import com.alibaba.druid.sql.PagerUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xpaas.common.config.CurrentUser;
import com.xpaas.common.utils.PageUtil;
import com.xpaas.core.secure.LoginUser;
import com.xpaas.home.dto.SxAddStudentCC;
import com.xpaas.home.vo.SxAddStudentRC;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;

import javax.validation.Valid;

import com.xpaas.core.mp.support.Condition;
import com.xpaas.core.mp.support.Query;
import com.xpaas.core.tool.api.R;
import com.xpaas.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.entity.StudentInfo;
import com.xpaas.home.vo.StudentInfoVO;
import com.xpaas.home.service.IStudentInfoService;
import com.xpaas.core.boot.ctrl.BaseController;

import java.util.List;

/**
 * 学员表 控制器
 *
 * @author xPaas
 * @since 2022-02-09
 */
@RestController
@AllArgsConstructor
@RequestMapping("/studentinfo")
@Api(value = "学员表", tags = "学员表接口")
public class StudentInfoController extends BaseController {

    private IStudentInfoService studentInfoService;

    /**
     * 详情
     */
    @GetMapping("/detail")
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "详情", notes = "传入studentInfo")
    public R<StudentInfo> detail(StudentInfo studentInfo) {
        StudentInfo detail = studentInfoService.getOne(Condition.getQueryWrapper(studentInfo));
        return R.data(detail);
    }

    /**
     * 分页 学员表
     */
    @GetMapping("/list")
    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "分页", notes = "传入studentInfo")
    public R<IPage<StudentInfo>> list(StudentInfo studentInfo, Query query) {
        IPage<StudentInfo> pages = studentInfoService.page(Condition.getPage(query), Condition.getQueryWrapper(studentInfo));
        return R.data(pages);
    }

    /**
     * 自定义分页 学员表
     */
    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "分页", notes = "传入studentInfo")
    public R<IPage<StudentInfoVO>> page(StudentInfoVO studentInfo, Query query) {
        IPage<StudentInfoVO> pages = studentInfoService.selectStudentInfoPage(Condition.getPage(query), studentInfo);
        return R.data(pages);
    }


    /**
     * 新增 学员表
     */
    @PostMapping("/save")
    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "新增", notes = "传入studentInfo")
    public R save(@Valid @RequestBody StudentInfo studentInfo) {
        return R.status(studentInfoService.save(studentInfo));
    }

    /**
     * 修改 学员表
     */
    @PostMapping("/update")
    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "修改", notes = "传入studentInfo")
    public R update(@Valid @RequestBody StudentInfo studentInfo) {
        return R.status(studentInfoService.updateById(studentInfo));
    }

    /**
     * 新增或修改 学员表
     */
    @PostMapping("/submit")
    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "新增或修改", notes = "传入studentInfo")
    public R submit(@Valid @RequestBody StudentInfo studentInfo) {
        return R.status(studentInfoService.saveOrUpdate(studentInfo));
    }


    /**
     * 删除 学员表
     */
    @PostMapping("/remove")
    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "删除", notes = "传入ids")
    public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
        return R.status(studentInfoService.removeByIds(Func.toLongList(ids)));
    }

    /**
     * 通过专业班次ID进行查询 所有学生
     */
    @PostMapping("/selectByTeam")
    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "通过学员队进行查询 所有学生", notes = "专业班次")
    public R selectByTeam(@RequestBody List<String> stringList) {

        LambdaQueryWrapper<StudentInfo> lambdaQuery = new LambdaQueryWrapper<>();
        lambdaQuery.in(StudentInfo::getSpecialtyClassId, stringList);
//		QueryWrapper queryWrapper=new QueryWrapper();
//		queryWrapper.eq("SPECIALTY_CLASS_ID", id);
        List list = studentInfoService.list(lambdaQuery);
        return R.data(list);
    }

    /**
     * 直接查询所有学生的信息
     */
    @PostMapping("/studentList")
    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "通过学员队进行查询 所有学生", notes = "专业班次")
    public R selectByTeam(@RequestBody StudentInfo studentInfo) {
        List<StudentInfo> list = studentInfoService.list(new QueryWrapper<StudentInfo>()
                .lambda().eq(studentInfo.getStudentCode() != null && !"".equals(studentInfo.getStudentCode())
                        , StudentInfo::getStudentCode, studentInfo.getStudentCode())
                .likeRight(studentInfo.getStudentName() != null && !"".equals(studentInfo.getStudentName())
                        , StudentInfo::getStudentName, studentInfo.getStudentName()));
        return R.data(list);
    }

    /**
     * 通过TOKEN获取当前登录用户的
     *
     * @param loginUser
     * @return
     */
    @GetMapping("/getCurrentOne")
    @ApiOperationSupport(order = 9)
    @ApiOperation(value = "获取当前登录学生用户的信息 ", notes = "")
    public R getCurrentOne(LoginUser loginUser) {
        String userId = loginUser.getUserId().toString();
        StudentInfo xpaasId = studentInfoService.getOne(
                new QueryWrapper<StudentInfo>()
                        .eq("XPAAS_ID", userId));
        return R.data(xpaasId);
    }

    @GetMapping("/getCurrentOneEntity")
    @ApiOperationSupport(order = 9)
    @ApiOperation(value = "获取当前登录学生用户的信息 ", notes = "")
    public StudentInfo getCurrentOneEntity(LoginUser loginUser) {
        String userId = loginUser.getUserId().toString();
        StudentInfo xpaasId = studentInfoService.getOne(
                new QueryWrapper<StudentInfo>()
                        .eq("XPAAS_ID", userId));
        return xpaasId;
    }

    /**
     * 通过专业班次ID进行查询 所有学生
     */
    @PostMapping("/selectByZybc")
    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "通过专业班次ID进行查询 所有学生", notes = "专业班次")
    public R selectByZybc(@RequestBody String zybcid) {

        LambdaQueryWrapper<StudentInfo> lambdaQuery = new LambdaQueryWrapper<>();
        lambdaQuery.eq(StudentInfo::getSpecialtyClassId, zybcid);
//		QueryWrapper queryWrapper=new QueryWrapper();
//		queryWrapper.eq("SPECIALTY_CLASS_ID", id);
        List<StudentInfo> list1 = studentInfoService.list(lambdaQuery);
        return R.data(list1);
    }

    /**
     * 根据planid查询学生
     */
    @PostMapping("/selectByplanid")
    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "根据planid查询学生", notes = "")
    public R selectByplanid(@RequestParam("planid") String[] planids) {


        List<StudentInfo> studentInfos = studentInfoService.selectStudentByPlanid(planids);


        return R.data(studentInfos);


    }

    @PostMapping("/selectByinfo")
    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "条件查询学生（施训选修课开课管理的添加学员查询）", notes = "")
    public R selectByinfo(@RequestBody SxAddStudentRC sxAddStudentRC,Query query) {

        List<SxAddStudentCC> querystudentinfobysxinfo = studentInfoService.querystudentinfobysxinfo(sxAddStudentRC);
        IPage<SxAddStudentCC> page = PageUtil.build().getPage(querystudentinfobysxinfo, query);

        return R.data(page);
    }


}
