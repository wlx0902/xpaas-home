package com.xpaas.home.controller;


import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.xpaas.core.tool.api.R;
import com.xpaas.home.entity.TeacherInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

@RequestMapping("/teacherinfo")
@Api(value = "公共接口", tags = "公共接口")
public class PublicController {




    /**
     * 查询数据数量
     */
    @PostMapping("/selectsjnum")
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "查询数据数量 ", notes = "")
    public R selectsjnum(@RequestBody List<T> list) {

        return R.data(list.size());

    }
}
