package com.xpaas.home.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;

import javax.validation.Valid;

import com.xpaas.core.mp.support.Condition;
import com.xpaas.core.mp.support.Query;
import com.xpaas.core.tool.api.R;
import com.xpaas.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.entity.Term;
import com.xpaas.home.vo.TermVO;
import com.xpaas.home.service.ITermService;
import com.xpaas.core.boot.ctrl.BaseController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 学期表 控制器
 *
 * @author xPaas
 * @since 2022-02-23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/term")
@Api(value = "学期表", tags = "学期表接口")
public class TermController extends BaseController {

    private ITermService termService;

    /**
     * 详情
     */
    @GetMapping("/detail")
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "详情", notes = "传入term")
    public R<Term> detail(Term term) {
        Term detail = termService.getOne(Condition.getQueryWrapper(term));
        return R.data(detail);
    }

    /**
     * 分页 学期表
     */
    @GetMapping("/list")
    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "分页", notes = "传入term")
    public R<IPage<Term>> list(Term term, Query query) {
        IPage<Term> pages = termService.page(Condition.getPage(query), Condition.getQueryWrapper(term));
        return R.data(pages);
    }

    /**
     * 自定义分页 学期表
     */
    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "分页", notes = "传入term")
    public R<IPage<TermVO>> page(TermVO term, Query query) {
        IPage<TermVO> pages = termService.selectTermPage(Condition.getPage(query), term);
        return R.data(pages);
    }


    /**
     * 新增 学期表
     */
    @PostMapping("/save")
    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "新增", notes = "传入term")
    public R save(@Valid @RequestBody Term term) {
        return R.status(termService.save(term));
    }

    /**
     * 修改 学期表
     */
    @PostMapping("/update")
    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "修改", notes = "传入term")
    public R update(@Valid @RequestBody Term term) {
        return R.status(termService.updateById(term));
    }

    /**
     * 新增或修改 学期表
     */
    @PostMapping("/submit")
    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "新增或修改", notes = "传入term")
    public R submit(@Valid @RequestBody Term term) {
        return R.status(termService.saveOrUpdate(term));
    }


    /**
     * 删除 学期表
     */
    @PostMapping("/remove")
    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "删除", notes = "传入ids")
    public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
        return R.status(termService.removeByIds(Func.toLongList(ids)));
    }


    /**
     * 获取当前学期
     *
     * @return
     */
    @GetMapping("/getCurrentTerm")
    public R getCurrentTerm() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
        String format = simpleDateFormat.format(new Date());
        String[] split = format.split("-");
        List<Term> terms = termService.list(new QueryWrapper<Term>()
                .lambda().eq(Term::getXqszxn, split[0])
                //"TO_CHAR(TO_DATE(XQKSRQ, 'YYYY-MM-DD')) BETWEEN TO_DATE({},'YYYY-MM-DD') AND TO_DATE({},'YYYY-MM-DD')"
                .apply("TO_CHAR(TO_DATE(XQKSRQ, 'YYYY-MM-DD')) < TO_DATE({0},'YYYY-MM-DD')",
                        format));
        if (terms == null) {
            List<Term> list = termService.list(new QueryWrapper<Term>()
                    .lambda().eq(Term::getXqszxn, split[0]));
            return R.data(list.get(0));
        } else if (terms.size() > 1) {
            return R.data(terms.get(1));
        }
        return R.data(terms);
    }

    /**
     * 获取所有学年
     *
     * @return
     */
    @GetMapping("/getCurrentTermStudyNian")
    public R getCurrentTermStudyNian() {

        QueryWrapper<Term> termQueryWrapper = new QueryWrapper<>();
        termQueryWrapper.select("XQSZXN");
        termQueryWrapper.groupBy("XQSZXN");
        termQueryWrapper.orderByDesc("XQSZXN");
        List<Term> list = termService.list(termQueryWrapper).stream().peek(term -> {
            if (term.getXqszxn().contains("-")) {
                term.setXqszxn(term.getXqszxn().split("-")[0]);
            }
        }).collect(Collectors.toList());
        return R.data(list);
    }


    /**
     * 获取所有学期
     *
     * @return
     */
    @GetMapping("/getCurrentTermAll")
    public R getCurrentTermAll() {

        QueryWrapper<Term> termQueryWrapper = new QueryWrapper<>();
        termQueryWrapper.orderByDesc("XQSZXN");
        termQueryWrapper.orderByDesc("XQXH");
        List<Term> list = termService.list(termQueryWrapper);
        return R.data(list);
    }

    /**
     * 根据学年获取对应的学期
     * @param year
     * @return
     */
    @GetMapping("/geteambygrade")
    public R geteambygrade(@RequestParam("year") String year){

        List<Term> list = termService.list(new QueryWrapper<Term>().lambda().eq(Term::getXqszxn, year));

        return R.data(list);

    }


    /**
     * 获取离 当前年份 最近前的10个学期
     *
     * @return
     */
    @GetMapping("/getCurrentTermTen")
    public R getCurrentTermTen() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(new Date());
        String[] split = format.split("-");
        int year = Integer.parseInt(split[0]);
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            stringList.add(String.valueOf(year - i));
        }
        List<Term> list = termService.list(new QueryWrapper<Term>()
                .lambda().in(stringList.size() > 0, Term::getXqszxn, stringList)
                .last("ORDER BY XQKSRQ desc"));
        return R.data(list);
    }


    /**
     * 根据当前学期查询 学期的详细信息
     *
     * @param term xqmc  传入学期名称
     * @return
     */
    @PostMapping("getTermDetail")
    public Term getTermDetail(@RequestBody Term term) {

        //根据传过来的当前学期查询（正常使用）
        Term one = termService.getOne(new QueryWrapper<Term>()
                .lambda().eq(Term::getXqmc, term.getXqmc()));
        return one;

        //写死的学期（测试数据）
//        Term one = termService.getOne(new QueryWrapper<Term>()
//                .lambda().eq(Term::getXqmc, "2021夏"));
//        return one;
    }
}
