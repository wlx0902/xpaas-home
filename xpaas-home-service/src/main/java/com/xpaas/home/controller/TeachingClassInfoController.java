package com.xpaas.home.controller;

import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import com.xpaas.core.mp.support.Condition;
import com.xpaas.core.mp.support.Query;
import com.xpaas.core.tool.api.R;
import com.xpaas.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.entity.TeachingClassInfo;
import com.xpaas.home.vo.TeachingClassInfoVO;
import com.xpaas.home.service.ITeachingClassInfoService;
import com.xpaas.core.boot.ctrl.BaseController;

import java.util.List;

/**
 * 教学班信息表 控制器
 *
 * @author xPaas
 * @since 2022-02-18
 */
@RestController
@AllArgsConstructor
@RequestMapping("/teachingclassinfo")
@Api(value = "教学班信息表", tags = "教学班信息表接口")
public class TeachingClassInfoController extends BaseController {

	private ITeachingClassInfoService teachingClassInfoService;

	/**
	 * 查询所有教学班
	 */
		@GetMapping("/selectAll")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "查询所有教学班", notes = "")
	public R selectAll() {
		List<TeachingClassInfoVO> teachingClassInfoVOS = teachingClassInfoService.selectByAll();
		return R.data(teachingClassInfoVOS);
	}

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入teachingClassInfo")
	public R<TeachingClassInfo> detail(TeachingClassInfo teachingClassInfo) {
		TeachingClassInfo detail = teachingClassInfoService.getOne(Condition.getQueryWrapper(teachingClassInfo));
		return R.data(detail);
	}

	/**
	 * 分页 教学班信息表
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入teachingClassInfo")
	public R<IPage<TeachingClassInfo>> list(TeachingClassInfo teachingClassInfo, Query query) {
		IPage<TeachingClassInfo> pages = teachingClassInfoService.page(Condition.getPage(query), Condition.getQueryWrapper(teachingClassInfo));
		return R.data(pages);
	}

    /**
     * 自定义分页 教学班信息表
     */
    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "分页", notes = "传入teachingClassInfo")
    public R<IPage<TeachingClassInfoVO>> page(TeachingClassInfoVO teachingClassInfo, Query query) {
        IPage<TeachingClassInfoVO> pages = teachingClassInfoService.selectTeachingClassInfoPage(Condition.getPage(query), teachingClassInfo);
        return R.data(pages);
    }



	/**
	 * 新增 教学班信息表
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入teachingClassInfo")
	public R save(@Valid @RequestBody TeachingClassInfo teachingClassInfo) {
		return R.status(teachingClassInfoService.save(teachingClassInfo));
	}

	/**
	 * 修改 教学班信息表
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入teachingClassInfo")
	public R update(@Valid @RequestBody TeachingClassInfo teachingClassInfo) {
		return R.status(teachingClassInfoService.updateById(teachingClassInfo));
	}

	/**
	 * 新增或修改 教学班信息表
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入teachingClassInfo")
	public R submit(@Valid @RequestBody TeachingClassInfo teachingClassInfo) {
		return R.status(teachingClassInfoService.saveOrUpdate(teachingClassInfo));
	}

	
	/**
	 * 删除 教学班信息表
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(teachingClassInfoService.removeByIds(Func.toLongList(ids)));
	}

	
}
