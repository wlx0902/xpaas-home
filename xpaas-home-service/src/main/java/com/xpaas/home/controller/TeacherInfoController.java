package com.xpaas.home.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xpaas.common.config.CurrentUser;
import com.xpaas.core.secure.LoginUser;
import com.xpaas.home.entity.StudentInfo;
import com.xpaas.home.service.impl.StudentInfoServiceImpl;
import com.xpaas.home.vo.StudentInfoVO;
import com.xpaas.home.vo.TeacherInfoVO2;
import com.xpaas.home.vo.TeacherInfoVO3;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;

import javax.validation.Valid;

import com.xpaas.core.mp.support.Condition;
import com.xpaas.core.mp.support.Query;
import com.xpaas.core.tool.api.R;
import com.xpaas.core.tool.utils.Func;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.entity.TeacherInfo;
import com.xpaas.home.vo.TeacherInfoVO;
import com.xpaas.home.service.ITeacherInfoService;
import com.xpaas.core.boot.ctrl.BaseController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 教员表 控制器
 *
 * @author xPaas
 * @since 2022-02-10
 */

@RestController
//@AllArgsConstructor
@RequestMapping("/teacherinfo")
@Api(value = "教员表", tags = "教员表接口")
public class TeacherInfoController extends BaseController {

    @Autowired
    private ITeacherInfoService teacherInfoService;

    /**
     * 查询所有教员
     */
    @GetMapping("/selectAll")
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "查询所有教员", notes = "")
    public R selectAll( ) {

        List<TeacherInfo> list = teacherInfoService.list();
        return R.data(list);


    }

    /**
     * 查询所有教员
     */
    @GetMapping("/selectAllName")
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "查询所有教员", notes = "")
    public R selectAllName( ) {

        List<TeacherInfo> list = teacherInfoService.list();
        List<String> stringList=new ArrayList<>();
        list.forEach((a)-> stringList.add(a.getXm()));
        return R.data(stringList);


    }

    /**
     * 详情
     */
    @GetMapping("/detail")
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "详情", notes = "传入teacherInfo")
    public R<TeacherInfo> detail(TeacherInfo teacherInfo) {
        TeacherInfo detail = teacherInfoService.getOne(Condition.getQueryWrapper(teacherInfo));
        return R.data(detail);
    }

    /**
     * 分页 教员表
     */
    @GetMapping("/list")
    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "分页", notes = "传入teacherInfo")
    public R<IPage<TeacherInfo>> list(TeacherInfo teacherInfo, Query query) {
        IPage<TeacherInfo> pages = teacherInfoService.page(Condition.getPage(query), Condition.getQueryWrapper(teacherInfo));
        return R.data(pages);
    }

    /**
     * 自定义分页 教员表
     */
    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "分页", notes = "传入teacherInfo")
    public R<IPage<TeacherInfoVO>> page(TeacherInfoVO teacherInfo, Query query) {
        IPage<TeacherInfoVO> pages = teacherInfoService.selectTeacherInfoPage(Condition.getPage(query), teacherInfo);
        return R.data(pages);
    }


    /**
     * 新增 教员表
     */
    @PostMapping("/save")
    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "新增", notes = "传入teacherInfo")
    public R save(@Valid @RequestBody TeacherInfo teacherInfo) {
        return R.status(teacherInfoService.save(teacherInfo));
    }

    /**
     * 修改 教员表
     */
    @PostMapping("/update")
    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "修改", notes = "传入teacherInfo")
    public R update(@Valid @RequestBody TeacherInfo teacherInfo) {
        return R.status(teacherInfoService.updateById(teacherInfo));
    }

    /**
     * 新增或修改 教员表
     */
    @PostMapping("/submit")
    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "新增或修改", notes = "传入teacherInfo")
    public R submit(@Valid @RequestBody TeacherInfo teacherInfo) {
        return R.status(teacherInfoService.saveOrUpdate(teacherInfo));
    }


    /**
     * 删除 教员表
     */
    @PostMapping("/remove")
    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "删除", notes = "传入ids")
    public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
        return R.status(teacherInfoService.removeByIds(Func.toLongList(ids)));
    }


    /**
     * 渲染职工列表
     */
    @RequestMapping(method = RequestMethod.POST, value = "/selectJob")
    public R selectJob(@RequestBody TeacherInfo teacherInfo) {
        List<TeacherInfo> teacherInfos = teacherInfoService.selectJobs(teacherInfo);
        List<TeacherInfoVO2> infoVOList = new ArrayList<>();
        for (TeacherInfo t : teacherInfos) {
            TeacherInfoVO2 teacherInfoVO = new TeacherInfoVO2();
            teacherInfoVO.setUnit(t.getBzbm());
            teacherInfoVO.setId(t.getId());
            teacherInfoVO.setName(t.getXm());
            teacherInfoVO.setSex(t.getXb());
            teacherInfoVO.setJob(t.getZw());
            infoVOList.add(teacherInfoVO);
        }
        return R.data(infoVOList);
    }

    /**
     * 渲染职工列表
     */
    @RequestMapping(method = RequestMethod.GET, value = "/selectJobName")
    public R selectJobName() {
        List<TeacherInfo> teacherInfos = teacherInfoService.list();
        List<TeacherInfoVO2> infoVOList = new ArrayList<>();
        for (TeacherInfo t : teacherInfos) {
            TeacherInfoVO2 teacherInfoVO = new TeacherInfoVO2();
            teacherInfoVO.setUnit(t.getBzbm());
            teacherInfoVO.setId(t.getId());
            teacherInfoVO.setName(t.getXm());
            teacherInfoVO.setSex(t.getXb());
            teacherInfoVO.setJob(t.getZw());
            infoVOList.add(teacherInfoVO);
        }
        return R.data(infoVOList);
    }

    /**
     * 根据主键进行查询ID
     */
    @GetMapping("/selectById")
    public TeacherInfo selectById(@RequestParam("id") Object id) {
        if (id != null)
            return teacherInfoService.getById((Serializable) id);
        return null;
    }

    /**
     * 根据主键进行查询ID
     */
    @GetMapping("/selectByIdStr")
    public TeacherInfo selectByIdStr(@RequestParam("id") String id) {
        if (id != null)
            return teacherInfoService.getOne(new QueryWrapper<TeacherInfo>().eq("XPAAS_ID", id));
        return null;
    }

    /**
     * 根据主键进行查询ID
     */
    @GetMapping("/selectByZjh")
    public TeacherInfo selectByZjh(@RequestParam("id") String id) {
            return teacherInfoService.getOne(new QueryWrapper<TeacherInfo>().eq("ZJHM", id));
    }

    /**
     * 根据Uid查询 XPAAS_ID
     */
    @GetMapping("/selectByUId")
    public TeacherInfo selectByUId(@RequestParam("uid") String uid) {
        if (uid != null) {
            QueryWrapper<TeacherInfo> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("U_ID", uid);
            return teacherInfoService.getOne(queryWrapper);
        }
        return null;
    }


    @Autowired
    private StudentInfoServiceImpl studentInfoService;

    /**
     * 获取当前登录用户的ID
     *
     * @param loginUser
     * @return
     */
    @GetMapping("/getCurrentOne")
    @ApiOperationSupport(order = 9)
    @ApiOperation(value = "获取当前登录教员用户的信息 ", notes = "无需传值 注解自动拦截获取值")
    public R getCurrentOne(@CurrentUser LoginUser loginUser) {
        if (loginUser == null)
            return R.data("当前无用户登录");
        if ("student".equals(loginUser.getRoleName())) {
            String userId = loginUser.getUserId().toString();
            StudentInfo xpaasId = studentInfoService.getOne(
                    new QueryWrapper<StudentInfo>()
                            .eq("XPAAS_ID", userId));
            return R.data(xpaasId);
        } else {
            String userId = loginUser.getUserId().toString();
            TeacherInfo xpaasId = teacherInfoService.getOne(
                    new QueryWrapper<TeacherInfo>()
                            .eq("XPAAS_ID", userId));
            return R.data(xpaasId);
        }
    }

    @GetMapping("/getCurrentOneEntity")
    @ApiOperationSupport(order = 9)
    @ApiOperation(value = "获取当前登录教员用户的信息 ", notes = "无需传值 注解自动拦截获取值")
    public TeacherInfo getCurrentOneEntity(@CurrentUser LoginUser loginUser) {
        if (loginUser == null) {
            return null;
        }
        String userId = loginUser.getUserId().toString();
        return teacherInfoService.getOne(
                new QueryWrapper<TeacherInfo>()
                        .eq("XPAAS_ID", userId));
    }






    /**
     * 渲染职工列表
     */
    @RequestMapping(method = RequestMethod.POST, value = "/selectDetailsJob")
    public R selectDetailsJob(@RequestBody TeacherInfo teacherInfo) {
        List<TeacherInfo> teacherInfos = teacherInfoService.selectJobs(teacherInfo);
        List<TeacherInfoVO3> infoVOList = new ArrayList<>();
        for (TeacherInfo t : teacherInfos) {
            TeacherInfoVO3 teacherInfoVO = new TeacherInfoVO3();
            teacherInfoVO.setUnit(t.getBzbm());
            teacherInfoVO.setId(t.getId());
            teacherInfoVO.setName(t.getXm());
            teacherInfoVO.setZjhm(t.getZjhm());
            teacherInfoVO.setJob(t.getZw());
            infoVOList.add(teacherInfoVO);
        }
        return R.data(infoVOList);
    }


    @GetMapping("/getteacherinfolistforteachroom")
    @ApiOperationSupport(order = 9)
    @ApiOperation(value = "根据教研室查询教员", notes = "")
    public R getteacherinfolistforteachroom(@RequestParam("teachroom") String teachroom,@RequestParam(value = "name",required = false) String name ) {

        QueryWrapper<TeacherInfo> q = new QueryWrapper();
        if (name!=null){
            q.eq("GZBM",teachroom);
            q.like("XM",name);

            List<TeacherInfo> list = teacherInfoService.list(q);
            return R.data(list);

        }else {
            q.eq("GZBM",teachroom);
            List<TeacherInfo> list = teacherInfoService.list(q);

            return R.data(list);
        }


    }

    @GetMapping("/getteacherinfolistteacher")
    @ApiOperationSupport(order = 9)
    @ApiOperation(value = "模糊查询教员", notes = "")
    public R getteacherinfolistteacher(@RequestParam(value = "name",required = false) String name ) {

        QueryWrapper<TeacherInfo> q = new QueryWrapper();
        if (name!=null){
            q.like("XM",name);

            List<TeacherInfo> list = teacherInfoService.list(q);
            return R.data(list);

        }else {

            return R.data(null);
        }


    }




}
