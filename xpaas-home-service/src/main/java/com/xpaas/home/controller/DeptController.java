package com.xpaas.home.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xpaas.common.config.CurrentUser;
import com.xpaas.core.secure.LoginUser;
import com.xpaas.home.entity.Dictory;
import com.xpaas.home.entity.TeacherInfo;
import com.xpaas.home.feign.GetRoleFeign;
import com.xpaas.home.feign.ListTeacherInfoFeign;
import com.xpaas.home.feign.RoleFeign;
import com.xpaas.home.service.IRoleService;
import com.xpaas.system.entity.DictBiz;
import com.xpaas.home.entity.Role;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import com.xpaas.core.mp.support.Condition;
import com.xpaas.core.mp.support.Query;
import com.xpaas.core.tool.api.R;
import com.xpaas.core.tool.utils.Func;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.entity.Dept;
import com.xpaas.home.vo.DeptVO;
import com.xpaas.home.service.IDeptService;
import com.xpaas.core.boot.ctrl.BaseController;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.List;

/**
 * 机构表 控制器
 *
 * @author xPaas
 * @since 2022-03-03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/dept")
@Api(value = "机构表", tags = "机构表接口")
public class DeptController extends BaseController {

	private IDeptService deptService;

	@Autowired
	GetRoleFeign roleFeign;
	@Autowired
	ListTeacherInfoFeign teacherInfoFeign;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入dept")
	public R<Dept> detail(Dept dept) {
		Dept detail = deptService.getOne(Condition.getQueryWrapper(dept));
		return R.data(detail);
	}

	/**
	 * 分页 机构表
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入dept")
	public R<IPage<Dept>> list(Dept dept, Query query) {
		IPage<Dept> pages = deptService.page(Condition.getPage(query), Condition.getQueryWrapper(dept));
		return R.data(pages);
	}

    /**
     * 自定义分页 机构表
     */
    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "分页", notes = "传入dept")
    public R<IPage<DeptVO>> page(DeptVO dept, Query query) {
        IPage<DeptVO> pages = deptService.selectDeptPage(Condition.getPage(query), dept);
        return R.data(pages);
    }



	/**
	 * 新增 机构表
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入dept")
	public R save(@Valid @RequestBody Dept dept) {
		return R.status(deptService.save(dept));
	}

	/**
	 * 修改 机构表
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入dept")
	public R update(@Valid @RequestBody Dept dept) {
		return R.status(deptService.updateById(dept));
	}

	/**
	 * 新增或修改 机构表
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入dept")
	public R submit(@Valid @RequestBody Dept dept) {
		return R.status(deptService.saveOrUpdate(dept));
	}


	/**
	 * 删除 机构表
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(deptService.removeByIds(Func.toLongList(ids)));
	}

	/**
	 * 通过id查到指定 depot
	 */
	@GetMapping("/selectById")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "通过id查到指定", notes = "传入id")
	public Dept selectById(@RequestParam String id) {
		 Dept byId = deptService.getById(id);
		return byId;
	}

	@GetMapping("/getCurrentOneEntity")
	@ApiOperationSupport(order = 9)
	@ApiOperation(value = "获取当前登录用户的部门信息 ", notes = "无需传值 注解自动拦截获取值")
	public Dept getCurrentOneEntity(LoginUser loginUser) {
		if (loginUser == null) {
			return null;
		}
		String userId = loginUser.getDeptId();

		return deptService.getOne(
				new QueryWrapper<Dept>()
						.eq("ID", userId));
	}

	@GetMapping("getxb")
	@ApiOperationSupport(order = 9)
	@ApiOperation(value = "获取系部 ", notes = "无需传值 注解自动拦截获取值")
	public R getbx(LoginUser loginUser) {

		Role queryrolename = roleFeign.queryrolename(loginUser.getRoleId());

		if (queryrolename.getRoleName().equals("系领导")){
			TeacherInfo currentOneEntity = teacherInfoFeign.getCurrentOneEntity();
			if (currentOneEntity!=null){

				QueryWrapper<Dept> q = new QueryWrapper<>();
				q.eq("DEPT_CATEGORY","5");
				q.eq("FULL_NAME",currentOneEntity.getGzbm());

				return R.data(deptService.list(q));
			}else {

				return R.data("没有找到该角色对应的人物");
			}
		}else if (queryrolename.getRoleName().equals("管理员")||queryrolename.getRoleName().equals("教科处领导")||queryrolename.getRoleName().equals("队长")||queryrolename.getRoleName().equals("大队长")){
			QueryWrapper<Dept> q =new QueryWrapper();

			q.eq("DEPT_CATEGORY","5");

			List<Dept>  list = deptService.list(q);

			return R.data(list);
		}

		return R.data(null);



	}

	/**
	 *  根据教学系查询教研室
	 */
	@GetMapping("querytclassroombyx")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "根据教学系查询教研室", notes = "传入ids")
	public R querytclassroombyx(@RequestParam("deptname") String name){


		QueryWrapper<Dept> q = new QueryWrapper();

		q.eq("DEPT_NAME",name);
		Dept one = deptService.getOne(q);

		QueryWrapper<Dept> q1 = new QueryWrapper();
		q1.eq("PARENT_ID",one.getId());

		List<Dept> list = deptService.list(q1);

		ArrayList<Dictory> dictories = new ArrayList<>();

		for (int i = 0; i <list.size() ; i++) {
			Dictory dictory = new Dictory();

			dictory.setCode(String.valueOf(list.get(i).getId()));
			dictory.setValue(list.get(i).getDeptName());

			dictories.add(dictory);
		}



		return R.data(dictories);



	}


	/**
	 *  查询所有教研室
	 */
	@GetMapping("querytclassroom")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "查询所有教研室", notes = "传入ids")
	public R querytclassroom(LoginUser loginUser){

		String roleId = loginUser.getRoleId();

		Role queryrolename = roleFeign.queryrolename(roleId);
		if (queryrolename.getRoleName().equals("教研室主任")){
			TeacherInfo currentOneEntity = teacherInfoFeign.getCurrentOneEntity();

			QueryWrapper<Dept> q = new QueryWrapper<>();
			q.eq("DEPT_CATEGORY","7");
			q.eq("FULL_NAME",currentOneEntity.getGzbm());
			return R.data(deptService.list(q));

		}else if (queryrolename.getRoleName().equals("系领导")){
			TeacherInfo currentOneEntity = teacherInfoFeign.getCurrentOneEntity();
			R querytclassroombyx = querytclassroombyx(currentOneEntity.getGzbm());
			return querytclassroombyx;
		}else if (queryrolename.getRoleName().equals("管理员")
				||queryrolename.getRoleName().equals("教科处领导")
				||queryrolename.getRoleName().equals("队长")
				||queryrolename.getRoleName().equals("大队长")
				||queryrolename.getRoleName().equals("系督导组长")
		){
			QueryWrapper<Dept> q = new QueryWrapper<>();
			q.eq("DEPT_CATEGORY","7");
			return R.data(deptService.list(q));

		}

     return R.data(null);

	}

	/**
	 *  查询当前登录用户的教研室
	 */
	@GetMapping("querytclassroomD")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "查询当前登录用户的教研室", notes = "传入ids")
	public List<Dept> querytclassroomD(LoginUser loginUser){


			TeacherInfo currentOneEntity = teacherInfoFeign.getCurrentOneEntity();

			QueryWrapper<Dept> q = new QueryWrapper<>();
			q.eq("DEPT_CATEGORY","7");
			q.eq("DEPT_NAME",currentOneEntity.getGzbm());
			return deptService.list(q);

	}

	/**
	 * 查询大队（队别）
	 */
	@GetMapping("querydb")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "查询所有大队(队别)", notes = "传入ids")
	public R querydb(LoginUser loginUser){

		Role queryrolename = roleFeign.queryrolename(loginUser.getRoleId());

		if (queryrolename.getRoleName().equals("大队长")){
			TeacherInfo currentOneEntity = teacherInfoFeign.getCurrentOneEntity();
			QueryWrapper<Dept> q = new QueryWrapper<>();
			q.eq("DEPT_CATEGORY","4");
			q.eq("FULL_NAME",currentOneEntity.getGzbm());
			return R.data(deptService.list(q));


		}else if (queryrolename.getRoleName().equals("管理员")||queryrolename.getRoleName().equals("教科处领导")){
			QueryWrapper<Dept> q = new QueryWrapper<>();
			q.eq("DEPT_CATEGORY","4");

			return R.data(deptService.list(q));
		}else {
			return R.data(null);
		}

	}

	/**
	 * 查询学员队
	 */
	@GetMapping("queryxyd")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "查询学员队", notes = "传入ids")
	public R queryxyd(@CurrentUser LoginUser loginUser){

		Role queryrolename = roleFeign.queryrolename(loginUser.getRoleId());
		if (queryrolename.getRoleName().equals("队长")){
			TeacherInfo currentOneEntity = teacherInfoFeign.getCurrentOneEntity();
			if (currentOneEntity!=null){

				QueryWrapper<Dept> q = new QueryWrapper<>();
				q.eq("DEPT_CATEGORY","6"); //打包的时候改成6
				q.eq("FULL_NAME",currentOneEntity.getGzbm());

				return R.data(deptService.list(q));
			}else {

				return R.data("没有找到该角色对应的人物");
			}
		}else if (queryrolename.getRoleName().equals("大队长")){
			TeacherInfo currentOneEntity = teacherInfoFeign.getCurrentOneEntity();
			if (currentOneEntity!=null){

				QueryWrapper<Dept> q = new QueryWrapper<>();
				q.eq("DEPT_CATEGORY","6"); //打包的时候改成6
				q.eq("FULL_NAME",currentOneEntity.getGzbm());

				return R.data(deptService.list(q));
			}else {

				return R.data("没有找到该角色对应的人物");
			}

		}else if (queryrolename.getRoleName().equals("管理员")
				||queryrolename.getRoleName().equals("教科处领导")
				||queryrolename.getRoleName().equals("教研室主任")

		){
			TeacherInfo currentOneEntity = teacherInfoFeign.getCurrentOneEntity();
			if (currentOneEntity!=null){
				QueryWrapper<Dept> q = new QueryWrapper<>();
				q.eq("DEPT_CATEGORY","6"); //打包的时候改成6
				return R.data(deptService.list(q));
			}else {

				return R.data("没有找到该角色对应的人物");
			}

		}
     return R.data(null);

	}

	/**
	 * 递归查询部门
	 */
	@PostMapping("/selectdgplace")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "递归查询部门", notes = "传入ids")
	public R selectdgplace() {

		List<Dept> depts = deptService.QueryDept();

//		ArrayList<DictBiz> objects = new ArrayList<>();
//
//		Iterator<Dept> iterator = depts.iterator();
//
//		while (iterator.hasNext()){
//
//			Dept next = iterator.next();
//			DictBiz dictBiz = new DictBiz();
//
//			dictBiz.
//
//
//		}

		return R.data(depts);

	}



	@GetMapping("/getDeptList")
	@ApiOperation("从组织机构表获取单位信息")
	@ApiOperationSupport(order = 10)
	public R getDeptList(){
		List<Dept> list = deptService.lambdaQuery().list();
		List<String> strings=new ArrayList<>();
		list.forEach(c->{
			strings.add(c.getDeptName());
		});
		return R.data(strings);
	}




}
