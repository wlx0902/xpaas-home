package com.xpaas.home.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xpaas.common.constant.ILaucher;
import com.xpaas.common.constant.LaucherEnum;
import com.xpaas.common.launch.PropertiesDev;
import com.xpaas.core.secure.utils.AuthUtil;
import com.xpaas.home.feign.RoleFeign;
import com.xpaas.home.mapper.UserMapper;
import com.xpaas.system.entity.Dept;
import com.xpaas.system.entity.Role;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.xpaas.core.mp.support.Condition;
import com.xpaas.core.mp.support.Query;
import com.xpaas.core.tool.api.R;
import com.xpaas.core.tool.utils.Func;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.entity.User;
import com.xpaas.home.vo.UserVO;
import com.xpaas.home.service.IUserService;
import com.xpaas.core.boot.ctrl.BaseController;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 用户表 控制器
 *
 * @author xPaas
 * @since 2022-02-28
 */
@RestController
//@AllArgsConstructor
@RequestMapping("/user")
@Api(value = "用户表", tags = "用户表接口")
public class UserController extends BaseController {

    @Autowired
    private IUserService userService;

    /**
     * 详情
     */
    @GetMapping("/detail")
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "详情", notes = "传入user")
    public R<User> detail(User user) {
        User detail = userService.getOne(Condition.getQueryWrapper(user));
        return R.data(detail);
    }

    /**
     * 分页 用户表
     */
    @GetMapping("/list")
    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "分页", notes = "传入user")
    public R<IPage<User>> list(User user, Query query) {
        IPage<User> pages = userService.page(Condition.getPage(query), Condition.getQueryWrapper(user));
        return R.data(pages);
    }

    /**
     * 自定义分页 用户表
     */
    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "分页", notes = "传入user")
    public R<IPage<UserVO>> page(UserVO user, Query query) {
        IPage<UserVO> pages = userService.selectUserPage(Condition.getPage(query), user);
        return R.data(pages);
    }


    /**
     * 新增 用户表
     */
    @PostMapping("/save")
    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "新增", notes = "传入user")
    public R save(@Valid @RequestBody User user) {
        return R.status(userService.save(user));
    }

    /**
     * 修改 用户表
     */
    @PostMapping("/update")
    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "修改", notes = "传入user")
    public R update(@Valid @RequestBody User user) {
        return R.status(userService.updateById(user));
    }

    /**
     * 新增或修改 用户表
     */
    @PostMapping("/submit")
    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "新增或修改", notes = "传入user")
    public R submit(@Valid @RequestBody User user) {
        return R.status(userService.saveOrUpdate(user));
    }


    /**
     * 删除 用户表
     */
    @PostMapping("/remove")
    @ApiOperationSupport(order = 8)
    @ApiOperation(value = "删除", notes = "传入ids")
    public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
        return R.status(userService.removeByIds(Func.toLongList(ids)));
    }
//---------------------------------------分隔线-----------------------------------------------------


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Value("${spring.cloud.nacos.discovery.server-addr}")
    private String nacosAddrs;

    /**
     * 根据角色ID 查询所有和这个角色有关的用户
     */
    @GetMapping("/userList")
    public R userList(@RequestParam(value = "roleId", required = false) String roleId) {
        if (roleId != null) {
            List<ServiceInstance> instances = discoveryClient.getInstances("xpaas-system");
            ServiceInstance instance = instances.get(0);
            URI uri = instance.getUri();
            String forObject = restTemplate.getForObject(uri + "/role/detail/?id=" + roleId, String.class);
            Role role = JSON.parseObject(JSON.parseObject(forObject, R.class)
                    .getData().toString(), Role.class);
            List<User> list = userService.list(new QueryWrapper<User>().lambda()
                    .like(roleId != null && !"".equals(roleId)
                            , User::getRoleId, roleId));
            list.forEach(r -> r.setRoleName(role.getRoleName()));
            return R.data(list);
        }
        return R.data(null);
    }


    @Autowired
    private UserMapper userMapper;

    /**
     * @param deptId
     * @return
     */
    @GetMapping("/getIdNumbers")
    public R getIdNumbers(@RequestParam("deptId") String deptId) {
        List<String> number = userMapper.getNumber(deptId);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < number.size(); i++) {
            if (i + 1 == number.size()) {
                sb.append(number.get(i));
            } else {
                sb.append(number.get(i)).append(",");
            }
        }
        return R.data(sb);
    }


    /**
     * 获取某个机构下的所有的教员的身份证号,入参机构id
     *
     * @param deptId
     * @param httpServletRequest
     * @return
     */
    @GetMapping("getPeopleNos")
    public R<String> getPeopleNos(@RequestParam("deptId") String deptId, HttpServletRequest httpServletRequest) {
        List<ServiceInstance> instances = discoveryClient.getInstances("xpaas-system");
        ServiceInstance instance = instances.get(0);
        URI uri = instance.getUri();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("xpaas-auth", AuthUtil.getHeader(httpServletRequest));
        HttpEntity<Object> objectHttpEntity;
        objectHttpEntity = new HttpEntity<>(httpHeaders);
        String body = restTemplate.exchange(uri + "/dept/lazy-list?parentId=" + deptId, HttpMethod.GET,
                objectHttpEntity, String.class).getBody();
        List<String> deptIds = null;
        StringBuilder sb = new StringBuilder();
        if (body != null) {
            List<Object> list = JSON.parseObject(JSON.parseObject(body, R.class)
                    .getData().toString(), List.class);
            deptIds = new ArrayList<>();
            for (Object o : list) {
                String string = JSON.parseObject(o.toString()
                        , Dept.class).getId().toString();
                deptIds.add(string);
            }
        }
        List<Object> objects = userService.listObjs(new QueryWrapper<User>()
                .lambda().select(User::getIdNumber).like(User::getDeptId, deptId));
        if (deptIds != null) {
            deptIds.forEach(l -> {
                List<Object> objects1 = userService.listObjs(new QueryWrapper<User>()
                        .lambda().select(User::getIdNumber).like(User::getDeptId, l));
                objects1.forEach(o -> {
                    if (!objects.contains(o)) {
                        objects.add(o);
                    }
                });
            });
        }
        for (int i = 0; i < objects.size(); i++) {
            if (i + 1 == objects.size()) {
                sb.append(objects.get(i));
            } else {
                sb.append(objects.get(i)).append(",");
            }
        }
        return R.data(sb.toString());
    }

    /**
     * 获取某个机构下的所有的教员的身份证号,入参机构id
     *
     * @param deptId
     * @param httpServletRequest
     * @return
     */
    @GetMapping("getPeopleNos1")
    public String getPeopleNos1(@RequestParam("deptId") String deptId, HttpServletRequest httpServletRequest) {
        List<ServiceInstance> instances = discoveryClient.getInstances("xpaas-system");
        ServiceInstance instance = instances.get(0);
        URI uri = instance.getUri();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("xpaas-auth", AuthUtil.getHeader(httpServletRequest));
        HttpEntity<Object> objectHttpEntity;
        objectHttpEntity = new HttpEntity<>(httpHeaders);
        String body = restTemplate.exchange(uri + "/dept/lazy-list?parentId=" + deptId, HttpMethod.GET,
                objectHttpEntity, String.class).getBody();
        List<String> deptIds = null;
        StringBuilder sb = new StringBuilder();
        if (body != null) {
            List<Object> list = JSON.parseObject(JSON.parseObject(body, R.class)
                    .getData().toString(), List.class);
            deptIds = new ArrayList<>();
            for (Object o : list) {
                String string = JSON.parseObject(o.toString()
                        , Dept.class).getId().toString();
                deptIds.add(string);
            }
        }
        List<Object> objects = userService.listObjs(new QueryWrapper<User>()
                .lambda().select(User::getIdNumber).like(User::getDeptId, deptId));
        if (deptIds != null) {
            deptIds.forEach(l -> {
                List<Object> objects1 = userService.listObjs(new QueryWrapper<User>()
                        .lambda().select(User::getIdNumber).like(User::getDeptId, l));
                objects1.forEach(o -> {
                    if (!objects.contains(o)) {
                        objects.add(o);
                    }
                });
            });
        }
        for (int i = 0; i < objects.size(); i++) {
            if (i + 1 == objects.size()) {
                sb.append(objects.get(i));
            } else {
                sb.append(objects.get(i)).append(",");
            }
        }
        return sb.toString();
    }



//    List<User> lambdaUsers3 = userMapper.selectList(new QueryWrapper<User>().lambda()
//            .nested(i -> i.eq(User::getRoleId, 2L).or().eq(User::getRoleId, 3L))
//            .and(i -> i.ge(User::getAge, 20)));



    /**
     * 获取某个机构下的所有的教员的身份证号,入参机构id
     */
//    @GetMapping("getPeopleNos")
//    public R getPeopleNos(@RequestParam("deptId") String deptId) {
//        List<Object> objects = userService.listObjs(new QueryWrapper<User>()
//                .lambda().select(User::getIdNumber).like(User::getDeptId, deptId));
//        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < objects.size(); i++) {
//            if (i + 1 == objects.size()) {
//                sb.append(objects.get(i));
//            } else {
//                sb.append(objects.get(i)).append(",");
//            }
//        }
//        return R.data(sb);
//    }
}
