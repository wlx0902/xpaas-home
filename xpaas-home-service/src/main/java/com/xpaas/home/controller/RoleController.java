package com.xpaas.home.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import com.xpaas.core.mp.support.Condition;
import com.xpaas.core.mp.support.Query;
import com.xpaas.core.tool.api.R;
import com.xpaas.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.entity.Role;
import com.xpaas.home.vo.RoleVO;
import com.xpaas.home.service.IRoleService;
import com.xpaas.core.boot.ctrl.BaseController;

/**
 * 角色表 控制器
 *
 * @author xPaas
 * @since 2022-05-19
 */
@RestController
@AllArgsConstructor
@RequestMapping("/role")
@Api(value = "角色表", tags = "角色表接口")
public class RoleController extends BaseController {

	private IRoleService roleService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入role")
	public R<Role> detail(Role role) {
		Role detail = roleService.getOne(Condition.getQueryWrapper(role));
		return R.data(detail);
	}

	/**
	 * 分页 角色表
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入role")
	public R<IPage<Role>> list(Role role, Query query) {
		IPage<Role> pages = roleService.page(Condition.getPage(query), Condition.getQueryWrapper(role));
		return R.data(pages);
	}

    /**
     * 自定义分页 角色表
     */
    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "分页", notes = "传入role")
    public R<IPage<RoleVO>> page(RoleVO role, Query query) {
        IPage<RoleVO> pages = roleService.selectRolePage(Condition.getPage(query), role);
        return R.data(pages);
    }



	/**
	 * 新增 角色表
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入role")
	public R save(@Valid @RequestBody Role role) {
		return R.status(roleService.save(role));
	}

	/**
	 * 修改 角色表
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入role")
	public R update(@Valid @RequestBody Role role) {
		return R.status(roleService.updateById(role));
	}

	/**
	 * 新增或修改 角色表
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入role")
	public R submit(@Valid @RequestBody Role role) {
		return R.status(roleService.saveOrUpdate(role));
	}

	
	/**
	 * 删除 角色表
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(roleService.removeByIds(Func.toLongList(ids)));
	}



	/**
	 * 删除 角色表
	 */
	@GetMapping("/queryrolename")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "获取角色姓名", notes = "传入ids")
	public Role queryrolename(String ids) {

		Role byId = roleService.getById(ids);

		return byId;

	}
	
}
