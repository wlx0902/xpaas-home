package com.xpaas.home.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import com.xpaas.core.mp.support.Condition;
import com.xpaas.core.mp.support.Query;
import com.xpaas.core.tool.api.R;
import com.xpaas.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xpaas.home.entity.UserDetails;
import com.xpaas.home.vo.UserDetailsVO;
import com.xpaas.home.service.IUserDetailsService;
import com.xpaas.core.boot.ctrl.BaseController;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户基本信息表 控制器
 *
 * @author xPaas
 * @since 2022-01-19
 */
@RestController
@AllArgsConstructor
@RequestMapping("/userdetails")
@Api(value = "用户基本信息表", tags = "用户基本信息表接口")
public class UserDetailsController extends BaseController {

	private IUserDetailsService userDetailsService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入userDetails")
	public R<UserDetails> detail(UserDetails userDetails) {
		UserDetails detail = userDetailsService.getOne(Condition.getQueryWrapper(userDetails));
		return R.data(detail);
	}

	/**
	 * 分页 用户基本信息表
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入userDetails")
	public R<IPage<UserDetails>> list(UserDetails userDetails, Query query) {
		IPage<UserDetails> pages = userDetailsService.page(Condition.getPage(query), Condition.getQueryWrapper(userDetails));
		return R.data(pages);
	}

    /**
     * 自定义分页 用户基本信息表
     */
    @GetMapping("/page")
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "分页", notes = "传入userDetails")
    public R<IPage<UserDetailsVO>> page(UserDetailsVO userDetails, Query query) {
        IPage<UserDetailsVO> pages = userDetailsService.selectUserDetailsPage(Condition.getPage(query), userDetails);
        return R.data(pages);
    }



	/**
	 * 新增 用户基本信息表
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入userDetails")
	public R save(@Valid @RequestBody UserDetails userDetails) {
		return R.status(userDetailsService.save(userDetails));
	}

	/**
	 * 修改 用户基本信息表
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入userDetails")
	public R update(@Valid @RequestBody UserDetails userDetails) {
		return R.status(userDetailsService.updateById(userDetails));
	}

	/**
	 * 新增或修改 用户基本信息表
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入userDetails")
	public R submit(@Valid @RequestBody UserDetails userDetails) {
		return R.status(userDetailsService.saveOrUpdate(userDetails));
	}

	
	/**
	 * 删除 用户基本信息表
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 8)
	@ApiOperation(value = "删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(userDetailsService.removeByIds(Func.toLongList(ids)));
	}


	@PostMapping("/selectJob")
	@ApiOperationSupport(order = 9)
	@ApiOperation(value = "渲染教职工列表", notes = "渲染前端教职工信息然后返回教职工列表")
	public List<UserDetails> selectJobs(@RequestBody(required = false) UserDetails userDetails){
		List<UserDetails> userDetailss = userDetailsService.selectJobs(userDetails);
		return userDetailss;
	}


	/**
	 * 根据id查询
	 */
	@RequestMapping(value = "/selectById",method = RequestMethod.GET)
	public UserDetails selectById(@RequestParam("id") Integer id){
		if (id!=null)
		return userDetailsService.getById(id);
		return null;
	}

	/**
	 * 根据Uid查询
	 */
	@GetMapping("/selectByUId")
	public UserDetails selectByUId(@RequestParam("uid") String uid){
		if (uid!=null){
			QueryWrapper queryWrapper=new QueryWrapper();
			queryWrapper.eq("U_ID", uid);
			return userDetailsService.getOne(queryWrapper);
		}
		return null;
	}

}
